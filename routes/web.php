<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index')->name('homepage');

Route::get('/aboutus', 'PageController@aboutus')->name('aboutus');
Route::get('/story', 'PageController@story')->name('story');
Route::get('/partners', 'PageController@partners')->name('partners');
Route::get('/gallery', 'PageController@gallery')->name('gallery');
Route::get('/spaces', 'PageController@spaces')->name('spaces');
Route::get('/contact', 'PageController@contact')->name('contact');
Route::get('/find-cocoon', 'PageController@find')->name('find');
Route::get('/login', 'PageController@login')->name('login');
Route::post('/send-mail', 'PageController@sendEmail')->name('send-mail');
Route::post('/add-subscriber', 'PageController@newsletterSubscriber')->name('add-subscriber');
Route::get('/events', 'EventsController@events')->name('events');
Route::get('/events/cocoon-curated-event-series/{id}', 'EventsController@viewRWCDetails')->name('rwc-event');
Route::post('/events/RWC/register-event', 'EventsController@registerRWCByEvent')->name('rwc-event-register');


Route::get('/insta', 'PageController@instagramfeed')->name('insta');


//SPACES
Route::get('/spaces/the-dining-room', 'FrontPortfolioController@room1')->name('salon1');
Route::get('/spaces/the-atrium', 'FrontPortfolioController@room2')->name('salon2');
Route::get('/spaces/the-formal-lounge', 'FrontPortfolioController@room3')->name('salon3');
Route::get('/spaces/jiangnam-tea-room', 'FrontPortfolioController@room4')->name('top1');
Route::get('/spaces/fornasetti-library', 'FrontPortfolioController@room5')->name('top2');
Route::get('/spaces/boardroom-tu', 'FrontPortfolioController@room6')->name('top3');
Route::get('/spaces/cocoon-bar', 'FrontPortfolioController@room7')->name('ground1');


//ADMIN ROUTINGS
Route::get('/hc-admin', 'AdminController@login')->name('admin-login');
Route::post('/auth/checklogin','AdminController@checklogin')->name('checklogin');
Route::get('/auth/logout','AdminController@logout')->name('logout');
Route::get('/admin/dashboard', 'DashboardController@index')->name('dashboard');

//Projects
Route::get('/admin/projects','ProjectsController@index')->name('projects');
Route::get('/admin/projects-add','ProjectsController@add')->name('projects-add');
Route::post('/admin/projects-add-action','ProjectsController@addAction')->name('projects-add-action');
Route::get('/admin/projects-category','ProjectsController@category')->name('projects-category');
Route::post('/admin/projects-add-category','ProjectsController@addCategory')->name('projects-add-category');

//Blogs
Route::get('/admin/blog-category','BlogsController@category')->name('blog-category');
Route::post('/admin/add-blog-category','BlogsController@addCategory')->name('add-blog-category');
