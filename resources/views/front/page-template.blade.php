<!DOCTYPE html>
<html lang="en">
<head>
@yield('title')
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="icon" type="image/png" sizes="16x16" href="{{ URL::asset('front-theme/images/icon.png')}}">
<meta name="keywords" content="Cocoon House,Cocoon House Auckland,Exclusive Events,Clubs in Auckland,Private Dining,Private Events,Private Party Bar,Chinese Tea Ceremony,Wine Bar Auckland,Elegant Cocktail Party,Wine Tasting Near Me,Private Dining Space,Cocoon House NZ,Cocoon House New Zealand,Event venue in Auckland,Event venue in NZ, Event venue in New Zealand">
<meta name="google-site-verification" content="6wITI-Vi8OcmBkYxl-xULNbtfcTl_RqaV8sflX5hWYw" />
@yield('meta');
<meta name="_token" content="{{csrf_token()}}" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/bootstrap-4.1.2/bootstrap.min.css')}}">
<link href="{{ URL::asset('front-theme/plugins/font-awesome-4.7.0/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('front-theme/plugins/colorbox/colorbox.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/plugins/OwlCarousel2-2.2.1/owl.carousel.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/plugins/OwlCarousel2-2.2.1/owl.theme.default.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/plugins/OwlCarousel2-2.2.1/animate.css')}}">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" type="text/css">

@yield('style')

</head>
<body>

<div class="super_container">

	<!-- Header -->

	<header class="header">
		<div class="header_content">

			<!-- Logo -->
			<div class="logo_container d-flex flex-column align-items-center justify-content-center">
				<div class="logo">
					<a href="#" class="text-center">
						<!-- <div class="logo_subtitle">hotel</div>
						<div class="logo_title">Samira</div>
						<div class="logo_stars">
							<ul class="d-flex flex-row align-items-start justfy-content-start">
								<li><i class="fa fa-star" aria-hidden="true"></i></li>
								<li><i class="fa fa-star" aria-hidden="true"></i></li>
								<li><i class="fa fa-star" aria-hidden="true"></i></li>
								<li><i class="fa fa-star" aria-hidden="true"></i></li>
								<li><i class="fa fa-star" aria-hidden="true"></i></li>
							</ul>
						</div> -->
						<img src="{{ URL::asset('front-theme/images/Cocoon-logo.png')}}" width="100px"/>
					</a>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="header_inner d-flex flex-row align-items-center justify-content-start">
							<nav class="main_nav">
								<ul class="d-flex flex-row align-items-center justify-content-start">
                                    <li @if(Session::get('nav')=='index') class="active" @endif><a href="{{ route('homepage') }}"><div class="nav_item d-flex flex-column align-items-center justify-content-center"><span>home</span></div></a></li>
									<li  class="@if(Session::get('nav')=='aboutus')active @endif dropdown" >
										<a href="{{ route('aboutus') }}" >
											<div class="nav_item d-flex flex-column align-items-center justify-content-center"><span>about</span></div>
										</a>
										<ul>
                                            <li><a href="{{ route('aboutus') }}">About</a></li>
                                            <li><a href="{{route('story')}}">Rosa's Story</a></li>
                                          </ul>
									</li>
                                    <li @if(Session::get('nav')=='spaces') class="active" @endif><a href="{{route('spaces')}}"><div class="nav_item d-flex flex-column align-items-center justify-content-center"><span>Cocoon Spaces</span></div></a></li>
                                    <li @if(Session::get('nav')=='gallery') class="active" @endif>
                                        <a href="{{route('gallery')}}">
                                            <div class="nav_item d-flex flex-column align-items-center justify-content-center"><span>ART + WINE + DESIGN</span></div>
                                        </a>
                                        <ul>
                                            <li><a href="{{route('gallery')}}">Art</a></li>
                                            <li><a href="{{route('events')}}">Events</a></li>
                                          </ul>
                                    </li>

                                    <li @if(Session::get('nav')=='partners') class="active" @endif><a href="{{route('partners')}}"><div class="nav_item d-flex flex-column align-items-center justify-content-center"><span>OUR PARTNERS</span></div></a></li>
                                    <li @if(Session::get('nav')=='contact') class="active" @endif>
                                        <a href="{{route('contact')}}">
                                            <div class="nav_item d-flex flex-column align-items-center justify-content-center"><span>contact</span></div>
                                        </a>
                                        <ul>
                                            <li><a href="{{ route('contact') }}">Contact</a></li>
                                            <li><a href="{{route('find')}}">Find Cocoon</a></li>
                                          </ul>
                                    </li>
								</ul>
							</nav>
                            <a href="{{route('login')}}" class="button_container header_button ml-auto"><div class="button text-center btn-header"><span>Member Login</span></div></a>
							<div class="hamburger ml-auto"><i class="fa fa-bars" aria-hidden="true"></i></div>
							<!-- <div class="header_review"><a href="#">Add your review</a></div> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<!-- Menu -->

	<div class="menu">
		<div class="background_image" style="background-image:url(images/menu.jpg)"></div>
		<div class="menu_content d-flex flex-column align-items-center justify-content-center">
			<ul class="menu_nav_list text-center">
				<li><a href="{{route('homepage')}}">Home</a></li>
				<li><a href="{{route('aboutus')}}">About</a></li>
                <li><a href="{{route('spaces')}}">Cocoon Spaces</a></li>
                <li><a href="{{route('story')}}">Rosa's Story</a></li>
                <li><a href="{{route('gallery')}}">Art + Wine + Design</a></li>
                <li><a href="{{route('events')}}">Events</a></li>
                <li><a href="{{route('partners')}}">Our Partners</a></li>
				<li><a href="{{route('find')}}">Find Cocoon</a></li>
				<li><a href="{{route('contact')}}">Contact</a></li>
			</ul>
		</div>
	</div>

    @yield('breadcrumb')

    <!-- Search Bar -->

    {{-- <div class="search_bar">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="search_bar_container">
                        <form action="#" id="search_bar_form" class="search_bar_form d-flex  clearfix">

                                <div class="col-md-9">
                                    <input type="text" class="search-input" placeholder="What are you looking for?" />
                                </div>
                                <div class="col-md-3">
                                    <button class="search_bar_button">Search</button>
                                </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    @yield('content')


	<!-- Newsletter -->

	<div class="newsletter" style="padding-top:20px;">
		<div class="container">
			<div class="row">

				<div class="col-lg-5">
					<div class="newsletter_content">
						<div class="section_title_container">
							<div class="section_subtitle">cocoon house</div>
							<div class="section_title"><h2>HOUSE NEWS</h2></div>
						</div>
						<div class="newsletter_text">
							<p>Subscribe to receive Cocoon House updates on upcoming events.</p>
						</div>
					</div>
				</div>
				<div class="col-lg-7">
					<div class="newsletter_form_container">
						<div  id="newsletter_form" class="newsletter_form">
							<input type="email" id="newsletter_email" class="newsletter_input" placeholder="Your e-mail" required="required">
							<button class="newsletter_button"><span>Subscribe</span></button>
                        </div>
					</div>
                </div>
                <div class="col-lg-12 message">

                </div>

			</div>
		</div>
		<div class="newsletter_border_container"><div class="container"><div class="row border_row"><div class="col"><div class="newsetter_border"></div></div></div></div></div>
    </div>

    <!-- Social -->
    <div style="width: 0px; height: 0px; position: absolute; left: -5000px;">
        <div id="cboxOverlay" style="display: none;"></div>
        <div id="st-3" class=" st-sticky-share-buttons st-right st-toggleable st-has-labels "><div class="st-btn st-first" data-network="facebook" style="display: inline-block;">
                <img src="https://platform-cdn.sharethis.com/img/facebook.svg">
                <span class="st-label">Share</span>
              </div><div class="st-btn" data-network="twitter" style="display: inline-block;">
                <img src="https://platform-cdn.sharethis.com/img/twitter.svg">
                <span class="st-label">Tweet</span>
              </div><div class="st-btn" data-network="pinterest" style="display: inline-block;">
                <img src="https://platform-cdn.sharethis.com/img/pinterest.svg">
                <span class="st-label">Pin</span>
              </div><div class="st-btn" data-network="email" style="display: inline-block;">
                <img src="https://platform-cdn.sharethis.com/img/email.svg">
                <span class="st-label">Email</span>
              </div><div class="st-btn" data-network="linkedin" style="display: inline-block;">
                <img src="https://platform-cdn.sharethis.com/img/linkedin.svg">
                <span class="st-label">Share</span>
              </div><div class="st-btn" data-network="sharethis" style="display: inline-block;">
                <img src="https://platform-cdn.sharethis.com/img/sharethis.svg">
                <span class="st-label">Share</span>
              </div><div class="st-btn st-last" data-network="sms" style="display: inline-block;">
                <img src="https://platform-cdn.sharethis.com/img/sms.svg">
                <span class="st-label">Share</span>
              </div><div class="st-toggle">
                <div class="st-left">
                  <img src="https://platform-cdn.sharethis.com/img/arrow_left.svg">
                </div>
                <div class="st-right">
                  <img src="https://platform-cdn.sharethis.com/img/arrow_right.svg">
                </div>
              </div></div>
    </div>
	<!-- Footer -->

	<footer class="footer">
		<div class="container">
			<div class="row">

				<!-- Footer Logo -->
				<div class="col-lg-4 footer_col">
					<div class="footer_logo_container">
						<div class="footer_logo">
							<a href="#" class="text-center">
								<img src="{{ URL::asset('front-theme/images/Cocoon-House-Gold.png')}}" width="150px"/>
							</a>
						</div>
						<div class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Cocoon House
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</div>
					</div>
				</div>

				<!-- Footer Menu -->
				<div class="col-lg-2 footer_col">
					<div class="footer_menu">
						<ul class="d-flex flex-row align-items-start justify-content-start" >
							{{-- <li><a href="#" class="fa fa-facebook"></a></li>
							<li><a href="#" class="fa fa-twitter"></a></li> --}}
                            <li><a href="https://www.instagram.com/cocoonhouse.nz/" class="fa fa-instagram" > <span style="font-family: 'Montserrat';font-size:20px;position: absolute;padding-left: 5px;padding-top: 3px;">cocoonhouse.nz</span></a></li>

						</ul>

					</div>
				</div>

				<!-- Footer Contact -->
				<div class="col-lg-6 footer_col">
					<div class="footer_contact clearfix">
						<div class="footer_contact_content float-lg-right">
							<ul>
								<li>Address: <span>35 Spring St, Freemans Bay, Auckland 1011, New Zealand</span></li>
								<li>Phone: <span>+64 274 904 152</span></li>
								<li>Email: <span><a href="mailto:shelley.arrell@cocoonhouse.co.nz">shelley.arrell@cocoonhouse.co.nz </a></span></li>
                            </ul>
                            <br>
                            <a href="{{ URL::asset('front-theme/downloadables/Cocoon House - Venue Hire Pack 2020_new.pdf')}}" target="_blank" class="button_container"><div class="button text-center btn-download"><span>Venue Hire Pack</span></div></a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</footer>
</div>

<script src="{{ URL::asset('front-theme/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{ URL::asset('front-theme/styles/bootstrap-4.1.2/popper.js')}}"></script>
<script src="{{ URL::asset('front-theme/styles/bootstrap-4.1.2/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('front-theme/plugins/greensock/TweenMax.min.js')}}"></script>
<script src="{{ URL::asset('front-theme/plugins/greensock/TimelineMax.min.js')}}"></script>
<script src="{{ URL::asset('front-theme/plugins/scrollmagic/ScrollMagic.min.js')}}"></script>
<script src="{{ URL::asset('front-theme/plugins/greensock/animation.gsap.min.js')}}"></script>
<script src="{{ URL::asset('front-theme/plugins/greensock/ScrollToPlugin.min.js')}}"></script>
<script src="{{ URL::asset('front-theme/plugins/colorbox/jquery.colorbox-min.js')}}"></script>
<script src="{{ URL::asset('front-theme/plugins/OwlCarousel2-2.2.1/owl.carousel.js')}}"></script>
<script src="{{ URL::asset('front-theme/plugins/easing/easing.js')}}"></script>
<script src="{{ URL::asset('front-theme/plugins/parallax-js-master/parallax.min.js')}}"></script>
<!—- ShareThis BEGIN -—>
<script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5d257dcc5dcabb0012a03d7e&product=custom-share-buttons"></script>
{{-- <script async src="https://platform-api.sharethis.com/js/sharethis.js#property=5d257dcc5dcabb0012a03d7e&product="sticky-share-buttons"></script> --}}
<script type="text/javascript">

    var newsletter_url = "{{route('add-subscriber')}}";
    $('.newsletter_button').click(function(e){
        if($('#newsletter_email').val() != ''){
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                    method: "POST",
                    url: newsletter_url,
                    data: {
                        email: $("#newsletter_email").val()
                    },
                    success: function(response) {
                        if(response==0){
                            $('.message').html(
                                '<div class="col-md-12 text-center wow animated fadeInUp newsletter-success">'+
                                    '<div class="alert alert-danger alert-block">'+
                                        '<strong>Cannot subscribe this time or this email is already an existing subscriber.</strong>'+
                                    '</div>'+
                                '</div>'
                            );
                        }else{
                            $('.message').html(
                                ' <div class="col-md-12 text-center wow animated fadeInUp newsletter-success">'+
                                    '<div class="alert alert-success alert-block">'+
                                        '<strong>You have successfully subscribed to Cocoon House newsletter.</strong>'+
                                    '</div>'+
                                '</div>'
                            );

                        }
                    },
                    error: function(response) {
                        console.log(response);
                    }
            });
        }
    });

    </script>
<!—- ShareThis END -—>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-146581545-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-146581545-1');
</script>


@yield('script')
</body>
</html>
