@extends('front.page-template')

@section('title')
<title>The Atrium - Cocoon House</title>
@endsection

@section('meta')
<meta name="description" content="Just beyond the walls of Beijing’s Imperial City is the hutong district famous for its interconnecting laneways and courtyard houses. Just like the ourtyards of the Hutong, Cocoon’s Atrium is a natural meeting point and social hub. A single birch tree reaches playfully up two stories and out to the sky and shimmering waters dance down the black rock wall and under the yard. ">
<link rel="canonical" href="{{route('salon2')}}">
<meta property="og:type" content="article">
<meta property="og:title" content="The Atrium - Cocoon House">
<meta property="og:description" content="Just beyond the walls of Beijing’s Imperial City is the hutong district famous for its interconnecting laneways and courtyard houses. Just like the ourtyards of the Hutong, Cocoon’s Atrium is a natural meeting point and social hub. A single birch tree reaches playfully up two stories and out to the sky and shimmering waters dance down the black rock wall and under the yard. ">
<meta property="og:image" content="{{ URL::asset('front-theme/images/the-atrium.jpg')}}">
<meta property="og:url" content="{{route('salon2')}}">
<meta property="og:site_name" content="Cocoon House">

<meta name="twitter:title" content="The Atrium - Cocoon House">
<meta name="twitter:description" content="Just beyond the walls of Beijing’s Imperial City is the hutong district famous for its interconnecting laneways and courtyard houses. Just like the ourtyards of the Hutong, Cocoon’s Atrium is a natural meeting point and social hub. A single birch tree reaches playfully up two stories and out to the sky and shimmering waters dance down the black rock wall and under the yard. ">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/the-atrium.jpg')}}">
<meta name="twitter:card" content="summary_large_image">
@endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about_responsive.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/lightslider.css')}}">
@endsection

@section('breadcrumb')
<div class="home">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{ URL::asset('front-theme/images/home_slider_2.jpg')}}" data-speed="0.8"></div>
		<div class="home_content">
			<div class="home_subtitle">Cocoon House</div>
			<div class="home_title">Room Details</div>
		</div>
    </div>
</div>
@endsection

@section('content')
<div class="intro">
    <div class="container">
        <div class="row">

            <!-- Intro Content -->
            <div class="col-lg-6 intro_col">
                <div class="intro_container1 d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">
                        <div class="intro_text1">
                            <h3>Atrium</h3>
                            <div class="col-lg-12">
                                <div class="row details-box">
                                    <table class="table">
                                        <tr>
                                            <td class="text-left"><i class="fa fa-cutlery"></i> &nbsp;Dining</td>
                                            <td class="text-right">20 guests</td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><i class="fa fa-glass"></i> &nbsp;Cocktail</td>
                                            <td class="text-right">30 guests</td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><i class="fa fa-ticket"></i> &nbsp;Theatre</td>
                                            <td class="text-right">20 guests</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div style="height: 20px;border-top:1px solid #efefef;"></div>
                            <p>
                                    Just beyond the walls of Beijing’s Imperial City is the hutong district famous for its interconnecting laneways and courtyard houses.
                                    Just like the ourtyards of the Hutong, Cocoon’s Atrium is a natural meeting point and social hub.
                                    A single birch tree reaches playfully up two stories and out to the sky and shimmering waters dance down the black rock wall and under the yard.
                                    The glass walls of the courtyard open fully on three sides, combining with the dining room and lounge to create a dramatic entertainment space.
                            </p>



                        </div>

                    </div>
                </div>
            </div>

            <!-- Intro Image -->
            <div class="col-lg-6 intro_col">
                <div class="intro_image magic_up">
                    <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/Atrium/1.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/Atrium/1.jpg')}}" />
                             </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/Atrium/2.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/Atrium/2.jpg')}}" />
                             </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/Atrium/3.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/Atrium/3.jpg')}}" />
                             </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/Atrium/4.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/Atrium/4.jpg')}}" />
                             </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/Atrium/5.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/Atrium/5.jpg')}}" />
                                </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/Atrium/6.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/Atrium/6.jpg')}}" />
                                </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/Atrium/7.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/Atrium/7.jpg')}}" />
                                </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/Atrium/8.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/Atrium/8.jpg')}}" />
                                </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/Atrium/9.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/Atrium/9.jpg')}}" />
                                </li>

                    </ul>
                </div>

            </div>

        </div>
    </div>
</div>


@endsection

@section('script')
<script src="{{ URL::asset('front-theme/js/about.js')}}"></script>
<script src="{{ URL::asset('front-theme/js/lightslider.js')}}"></script>
<script>
        $(document).ready(function() {
           $("#content-slider").lightSlider({
               loop:true,
               keyPress:true
           });
           $('#image-gallery').lightSlider({
               gallery:true,
               item:1,
               thumbItem:4,
               slideMargin: 0,
               speed:1500,
               auto:true,
               loop:true,
               onSliderLoad: function() {
                   $('#image-gallery').removeClass('cS-hidden');
               }
           });
       });
   </script>
@endsection
