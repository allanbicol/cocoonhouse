@extends('front.page-template')

@section('title')
<title>The Dining Room - Cocoon House</title>
@endsection

@section('meta')
<meta name="description" content="Opening to the west of the atrium is a relaxed kitchen and dining room featuring a giant swamp Kauri dining table for 18 guests. The table was specially commissioned for the salon and is as much art as furniture. Sitting at the table guests are cloaked in beauty. A dramatic John Pule Tapa cloth hangs above the fire place looking across to delights from the Cocoon sculpture collection on the opposite wall.">
<link rel="canonical" href="{{route('salon1')}}">
<meta property="og:type" content="article">
<meta property="og:title" content="The Dining Room - Cocoon House">
<meta property="og:description" content="Opening to the west of the atrium is a relaxed kitchen and dining room featuring a giant swamp Kauri dining table for 18 guests. The table was specially commissioned for the salon and is as much art as furniture. Sitting at the table guests are cloaked in beauty. A dramatic John Pule Tapa cloth hangs above the fire place looking across to delights from the Cocoon sculpture collection on the opposite wall.">
<meta property="og:image" content="{{ URL::asset('front-theme/images/the-dining-room.jpg')}}">
<meta property="og:url" content="{{route('salon1')}}">
<meta property="og:site_name" content="Cocoon House">

<meta name="twitter:title" content="The Dining Room - Cocoon House">
<meta name="twitter:description" content="Opening to the west of the atrium is a relaxed kitchen and dining room featuring a giant swamp Kauri dining table for 18 guests. The table was specially commissioned for the salon and is as much art as furniture. Sitting at the table guests are cloaked in beauty. A dramatic John Pule Tapa cloth hangs above the fire place looking across to delights from the Cocoon sculpture collection on the opposite wall.">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/the-dining-room.jpg')}}">
<meta name="twitter:card" content="summary_large_image">
@endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about_responsive.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/lightslider.css')}}">
@endsection

@section('breadcrumb')
<div class="home">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{ URL::asset('front-theme/images/home_slider_2.jpg')}}" data-speed="0.8"></div>
		<div class="home_content">
			<div class="home_subtitle">Cocoon House</div>
			<div class="home_title">Room Details</div>
		</div>
    </div>
</div>
@endsection

@section('content')
<div class="intro">
    <div class="container">
        <div class="row">

            <!-- Intro Content -->
            <div class="col-lg-6 intro_col">
                <div class="intro_container1 d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">
                        <div class="intro_text1">
                            <h3>Dining Room</h3>
                            <div class="col-lg-12">
                                <div class="row details-box">
                                    <table class="table">
                                        {{-- <tr>
                                            <td class="text-left"><i class="fa fa-money"></i> &nbsp;Price</td>
                                            <td class="text-right">$1,500.00 + gst <div style="font-size:8px;color:#000;">(includes Mixologist)</td>
                                        </tr> --}}
                                        <tr>
                                            <td class="text-left"><i class="fa fa-cutlery"></i> &nbsp;Dining</td>
                                            <td class="text-right">30 guests</td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><i class="fa fa-briefcase"></i> &nbsp;Boardroom</td>
                                            <td class="text-right">18 guests</td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><i class="fa fa-glass"></i> &nbsp;Cocktail</td>
                                            <td class="text-right">40 guests</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div style="height: 50px;border-top:1px solid #efefef;"></div>
                            <p>
                                    Opening to the west of the atrium is a relaxed kitchen and dining room featuring a giant swamp Kauri dining table for 18 guests.
                                    The table was specially commissioned for the salon and is as much art as furniture.
                                    Sitting at the table guests are cloaked in beauty.
                                    A dramatic John Pule Tapa cloth hangs above the fire place looking across to delights from the Cocoon sculpture collection on the opposite wall.
                            </p>

                            <div class="blank-space"></div>
                        <a href="mailto:info@cocoonhouse.co.nz?subject=Cocoon House - Dining Room Booking" class="button_container room_button "><div class="button text-center"><span>Book Now</span></div></a>
                        </div>

                    </div>
                </div>
            </div>

            <!-- Intro Image -->
            <div class="col-lg-6 intro_col">
                <div class="intro_image magic_up">
                    {{-- <img src="{{ URL::asset('front-theme/images/the-dining-room.jpg')}}" width="100%" alt=""> --}}
                    <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                            <li data-thumb="{{ URL::asset('front-theme/images/spaces/Dining/1.jpg')}}">
                                <img src="{{ URL::asset('front-theme/images/spaces/Dining/1.jpg')}}" />
                                 </li>
                            <li data-thumb="{{ URL::asset('front-theme/images/spaces/Dining/2.jpg')}}">
                                <img src="{{ URL::asset('front-theme/images/spaces/Dining/2.jpg')}}" />
                                 </li>
                            <li data-thumb="{{ URL::asset('front-theme/images/spaces/Dining/3.jpg')}}">
                                <img src="{{ URL::asset('front-theme/images/spaces/Dining/3.jpg')}}" />
                                 </li>
                            <li data-thumb="{{ URL::asset('front-theme/images/spaces/Dining/4.jpg')}}">
                                <img src="{{ URL::asset('front-theme/images/spaces/Dining/4.jpg')}}" />
                                 </li>
                            <li data-thumb="{{ URL::asset('front-theme/images/spaces/Dining/5.jpg')}}">
                                <img src="{{ URL::asset('front-theme/images/spaces/Dining/5.jpg')}}" />
                                    </li>
                            <li data-thumb="{{ URL::asset('front-theme/images/spaces/Dining/6.jpg')}}">
                                <img src="{{ URL::asset('front-theme/images/spaces/Dining/6.jpg')}}" />
                                    </li>
                            <li data-thumb="{{ URL::asset('front-theme/images/spaces/Dining/7.jpg')}}">
                                <img src="{{ URL::asset('front-theme/images/spaces/Dining/7.jpg')}}" />
                                    </li>
                            <li data-thumb="{{ URL::asset('front-theme/images/spaces/Dining/8.jpg')}}">
                                <img src="{{ URL::asset('front-theme/images/spaces/Dining/8.jpg')}}" />
                                    </li>
                            <li data-thumb="{{ URL::asset('front-theme/images/spaces/Dining/9.jpg')}}">
                                <img src="{{ URL::asset('front-theme/images/spaces/Dining/9.jpg')}}" />
                                    </li>
                            <li data-thumb="{{ URL::asset('front-theme/images/spaces/Dining/10.jpg')}}">
                                <img src="{{ URL::asset('front-theme/images/spaces/Dining/10.jpg')}}" />
                                    </li>
                            <li data-thumb="{{ URL::asset('front-theme/images/spaces/Dining/11.jpg')}}">
                                <img src="{{ URL::asset('front-theme/images/spaces/Dining/11.jpg')}}" />
                                    </li>
                            <li data-thumb="{{ URL::asset('front-theme/images/spaces/Dining/12.jpg')}}">
                                <img src="{{ URL::asset('front-theme/images/spaces/Dining/12.jpg')}}" />
                                    </li>
                        </ul>
                </div>
            </div>

        </div>
    </div>
</div>


@endsection

@section('script')
<script src="{{ URL::asset('front-theme/js/about.js')}}"></script>
<script src="{{ URL::asset('front-theme/js/lightslider.js')}}"></script>
<script>
        $(document).ready(function() {
           $("#content-slider").lightSlider({
               loop:true,
               keyPress:true
           });
           $('#image-gallery').lightSlider({
               gallery:true,
               item:1,
               thumbItem:4,
               slideMargin: 0,
               speed:1500,
               auto:true,
               loop:true,
               onSliderLoad: function() {
                   $('#image-gallery').removeClass('cS-hidden');
               }
           });
       });
   </script>
@endsection
