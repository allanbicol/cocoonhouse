@extends('front.page-template')

@section('title')
<title>BOARDROOM TU - Cocoon House</title>
@endsection

@section('meta')
<meta name="description" content="Maori god Tu takes pride of place in this elegant alcove, looking out from the John Walsh painting Pare O Tūmatauenga. The Tu boardroom is equipped with an AV projection and is Ideal for small board meetings, break out groups, or uninterrupted work.">
<link rel="canonical" href="{{route('top3')}}">
<meta property="og:type" content="article">
<meta property="og:title" content="BOARDROOM TU - Cocoon House">
<meta property="og:description" content="Maori god Tu takes pride of place in this elegant alcove, looking out from the John Walsh painting Pare O Tūmatauenga. The Tu boardroom is equipped with an AV projection and is Ideal for small board meetings, break out groups, or uninterrupted work.">
<meta property="og:image" content="{{ URL::asset('front-theme/images/boardroom-tu.jpg')}}">
<meta property="og:url" content="{{route('top3')}}">
<meta property="og:site_name" content="Cocoon House">

<meta name="twitter:title" content="BOARDROOM TU - Cocoon House">
<meta name="twitter:description" content="Maori god Tu takes pride of place in this elegant alcove, looking out from the John Walsh painting Pare O Tūmatauenga. The Tu boardroom is equipped with an AV projection and is Ideal for small board meetings, break out groups, or uninterrupted work.">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/boardroom-tu.jpg')}}">
<meta name="twitter:card" content="summary_large_image">
@endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about_responsive.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/lightslider.css')}}">
@endsection

@section('breadcrumb')
<div class="home">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{ URL::asset('front-theme/images/home_slider_2.jpg')}}" data-speed="0.8"></div>
		<div class="home_content">
			<div class="home_subtitle">Cocoon House</div>
			<div class="home_title">Room Details</div>
		</div>
    </div>
</div>
@endsection

@section('content')
<div class="intro">
    <div class="container">
        <div class="row">

            <!-- Intro Content -->
            <div class="col-lg-6 intro_col">
                <div class="intro_container1 d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">
                        <div class="intro_text1">
                            <h3>BOARDROOM TU</h3>
                            <div class="col-lg-12">
                                <div class="row details-box">
                                    <table class="table">
                                        <tr>
                                            <td class="text-left"><i class="fa fa-money"></i> &nbsp;Price</td>
                                            <td class="text-right">$75+gst per hour</td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><i class="fa fa-cutlery"></i> &nbsp;Dining</td>
                                            <td class="text-right">8 guests</td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><i class="fa fa-briefcase"></i> &nbsp;Meeting</td>
                                            <td class="text-right">8 guests</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div style="height: 50px;border-top:1px solid #efefef;"></div>
                            <p>
                                Named Tu, after Tūmatauenga, the Maori god of war, hunting and food cultivation.
                                <br><br>
                                Maori god Tu takes pride of place in this elegant alcove, looking out from the John Walsh painting Pare O Tūmatauenga.
                                The Tu boardroom is equipped with an AV projection and is Ideal for small board meetings, break out groups, or uninterrupted work.
                            </p>


                        </div>
                        <div class="blank-space"></div>
                        <a href="mailto:info@cocoonhouse.co.nz?subject=Cocoon House - Boardroom Tu Booking" class="button_container room_button "><div class="button text-center"><span>Book Now</span></div></a>
                    </div>
                </div>
            </div>

            <!-- Intro Image -->
            <div class="col-lg-6 intro_col">
                <div class="intro_image magic_up">
                    <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/BoardroomTu/1.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/BoardroomTu/1.jpg')}}" />
                             </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/BoardroomTu/2.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/BoardroomTu/2.jpg')}}" />
                             </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/BoardroomTu/3.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/BoardroomTu/3.jpg')}}" />
                             </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/BoardroomTu/4.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/BoardroomTu/4.jpg')}}" />
                             </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/BoardroomTu/5.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/BoardroomTu/5.jpg')}}" />
                                </li>


                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>


@endsection

@section('script')
<script src="{{ URL::asset('front-theme/js/about.js')}}"></script>
<script src="{{ URL::asset('front-theme/js/lightslider.js')}}"></script>
<script>
        $(document).ready(function() {
           $("#content-slider").lightSlider({
               loop:true,
               keyPress:true
           });
           $('#image-gallery').lightSlider({
               gallery:true,
               item:1,
               thumbItem:4,
               slideMargin: 0,
               speed:1500,
               auto:true,
               loop:true,
               onSliderLoad: function() {
                   $('#image-gallery').removeClass('cS-hidden');
               }
           });
       });
   </script>
@endsection
