@extends('front.page-template')

@section('title')
<title>COCOON BAR - Cocoon House</title>
@endsection

@section('meta')
<meta name="description" content="Tucked snuggly behind the Salon staircase, this most private escape has no windows to the outside world. Time has been known to stand completely still in this space. Feeling very much like a real cocoon, guests of the bar are protected from the outside world by a luxurious cloak of art, exotic whiskey and the finest wines.">
<link rel="canonical" href="{{route('ground1')}}">
<meta property="og:type" content="article">
<meta property="og:title" content="COCOON BAR - Cocoon House">
<meta property="og:description" content="Tucked snuggly behind the Salon staircase, this most private escape has no windows to the outside world. Time has been known to stand completely still in this space. Feeling very much like a real cocoon, guests of the bar are protected from the outside world by a luxurious cloak of art, exotic whiskey and the finest wines.">
<meta property="og:image" content="{{ URL::asset('front-theme/images/cocoon-bar.jpg')}}">
<meta property="og:url" content="{{route('ground1')}}">
<meta property="og:site_name" content="Cocoon House">

<meta name="twitter:title" content="COCOON BAR - Cocoon House">
<meta name="twitter:description" content="Tucked snuggly behind the Salon staircase, this most private escape has no windows to the outside world. Time has been known to stand completely still in this space. Feeling very much like a real cocoon, guests of the bar are protected from the outside world by a luxurious cloak of art, exotic whiskey and the finest wines.">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/cocoon-bar.jpg')}}">
<meta name="twitter:card" content="summary_large_image">
@endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about_responsive.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/lightslider.css')}}">
@endsection

@section('breadcrumb')
<div class="home">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{ URL::asset('front-theme/images/home_slider_2.jpg')}}" data-speed="0.8"></div>
		<div class="home_content">
			<div class="home_subtitle">Cocoon House</div>
			<div class="home_title">Room Details</div>
		</div>
    </div>
</div>
@endsection

@section('content')
<div class="intro">
    <div class="container">
        <div class="row">

            <!-- Intro Content -->
            <div class="col-lg-6 intro_col">
                <div class="intro_container1 d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">
                        <div class="intro_text1">
                            <h3>COCOON BAR</h3>
                            <div class="col-lg-12">
                                <div class="row details-box">
                                    <table class="table">
                                        <tr>
                                            <td class="text-left"><i class="fa fa-money"></i> &nbsp;Price</td>
                                            <td class="text-right">$1,500.00 + gst <div style="font-size:8px;color:#000;">(includes Mixologist)</td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><i class="fa fa-user"></i> &nbsp;Cocktail</td>
                                            <td class="text-right">20 guests</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div style="height: 50px;border-top:1px solid #efefef;"></div>
                            <p>
                                The lavish but ever so intimate Cocoon Bar is a hidden treasure of the house.
                                <br><br>
                                Tucked snuggly behind the Salon staircase, this most private escape has no windows to the outside world.
                                Time has been known to stand completely still in this space.
                                Feeling very much like a real cocoon, guests of the bar are protected from the outside world by a luxurious cloak of art, exotic whiskey and the finest wines.
                            </p>


                        </div>
                        <div class="blank-space"></div>
                        <a href="mailto:info@cocoonhouse.co.nz?subject=Cocoon House - Cocoon Bar Booking" class="button_container room_button "><div class="button text-center"><span>Book Now</span></div></a>
                    </div>
                </div>
            </div>

            <!-- Intro Image -->
            <div class="col-lg-6 intro_col">
                <div class="intro_image magic_up">
                    <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/CocoonBar/1.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/CocoonBar/1.jpg')}}" />
                             </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/CocoonBar/2.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/CocoonBar/2.jpg')}}" />
                             </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/CocoonBar/3.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/CocoonBar/3.jpg')}}" />
                             </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/CocoonBar/4.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/CocoonBar/4.jpg')}}" />
                             </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/CocoonBar/5.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/CocoonBar/5.jpg')}}" />
                                </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/CocoonBar/6.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/CocoonBar/6.jpg')}}" />
                                </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/CocoonBar/7.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/CocoonBar/7.jpg')}}" />
                                </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/CocoonBar/8.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/CocoonBar/8.jpg')}}" />
                                </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/CocoonBar/9.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/CocoonBar/9.jpg')}}" />
                                </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/CocoonBar/10.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/CocoonBar/10.jpg')}}" />
                                </li>

                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>


@endsection

@section('script')
<script src="{{ URL::asset('front-theme/js/about.js')}}"></script>
<script src="{{ URL::asset('front-theme/js/lightslider.js')}}"></script>
<script>
        $(document).ready(function() {
           $("#content-slider").lightSlider({
               loop:true,
               keyPress:true
           });
           $('#image-gallery').lightSlider({
               gallery:true,
               item:1,
               thumbItem:4,
               slideMargin: 0,
               speed:1500,
               auto:true,
               loop:true,
               onSliderLoad: function() {
                   $('#image-gallery').removeClass('cS-hidden');
               }
           });
       });
   </script>
@endsection
