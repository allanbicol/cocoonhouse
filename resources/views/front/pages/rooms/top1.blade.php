@extends('front.page-template')

@section('title')
<title>JIANGNAN TEA ROOM - Cocoon House</title>
@endsection

@section('meta')
<meta name="description" content="Two stunning Ming period replica chairs were specially commissioned for the room and took 8 months to produce. Rosa matches these masterpieces of minimalist design with a large-scale print of the famous Jiangnan water village. This cultural treasure by Wu Guangzhong sits behind the Tea master radiating timeless serenity to all those gathered at the tea table. ">
<link rel="canonical" href="{{route('top1')}}">
<meta property="og:type" content="article">
<meta property="og:title" content="JIANGNAN TEA ROOM - Cocoon House">
<meta property="og:description" content="Two stunning Ming period replica chairs were specially commissioned for the room and took 8 months to produce. Rosa matches these masterpieces of minimalist design with a large-scale print of the famous Jiangnan water village. This cultural treasure by Wu Guangzhong sits behind the Tea master radiating timeless serenity to all those gathered at the tea table. ">
<meta property="og:image" content="{{ URL::asset('front-theme/images/jiangnam-tea-room.jpg')}}">
<meta property="og:url" content="{{route('top1')}}">
<meta property="og:site_name" content="Cocoon House">

<meta name="twitter:title" content="JIANGNAN TEA ROOM - Cocoon House">
<meta name="twitter:description" content="Two stunning Ming period replica chairs were specially commissioned for the room and took 8 months to produce. Rosa matches these masterpieces of minimalist design with a large-scale print of the famous Jiangnan water village. This cultural treasure by Wu Guangzhong sits behind the Tea master radiating timeless serenity to all those gathered at the tea table. ">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/jiangnam-tea-room.jpg')}}">
<meta name="twitter:card" content="summary_large_image">
@endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about_responsive.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/lightslider.css')}}">
@endsection

@section('breadcrumb')
<div class="home">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{ URL::asset('front-theme/images/home_slider_2.jpg')}}" data-speed="0.8"></div>
		<div class="home_content">
			<div class="home_subtitle">Cocoon House</div>
			<div class="home_title">Room Details</div>
		</div>
    </div>
</div>
@endsection

@section('content')
<div class="intro">
    <div class="container">
        <div class="row">

            <!-- Intro Content -->
            <div class="col-lg-6 intro_col">
                <div class="intro_container1 d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">
                        <div class="intro_text1">
                            <h3>JIANGNAN TEA ROOM</h3>
                            <div class="col-lg-12">
                                <div class="row details-box">
                                    <table class="table">
                                        <tr>
                                            <td class="text-left"><i class="fa fa-money"></i> &nbsp;Price</td>
                                            <td class="text-right">$800.00 + gst<br><div style="font-size:8px;color:#000;">+ tea selections (inc. Tea Master)</td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><i class="fa fa-user"></i> &nbsp;Seated</td>
                                            <td class="text-right">8 guests</td>
                                        </tr>
                                    </table>

                                </div>
                            </div>
                            <div style="height: 50px;border-top:1px solid #efefef;"></div>
                            <p>
                                Two stunning Ming period replica chairs were specially commissioned for the room and took 8 months to produce.
                                Rosa matches these masterpieces of minimalist design with a large-scale print of the famous Jiangnan water village.
                                This cultural treasure by Wu Guangzhong sits behind the Tea master radiating timeless serenity to all those gathered at the tea table.
                                The table is another bespoke piece crafted from swamp kauri and usually laden with exquisite tea pots and tea accoutrement.
                                Guests seated at the table enjoy a remarkable view through the branches of the courtyard birch tree across to the Fornasetti Library.
                            </p>


                        </div>
                        <div class="blank-space"></div>
                        <a href="mailto:info@cocoonhouse.co.nz?subject=Cocoon House - Jiangnan Tea Room Booking" class="button_container room_button "><div class="button text-center"><span>Book Now</span></div></a>
                    </div>
                </div>
            </div>

            <!-- Intro Image -->
            <div class="col-lg-6 intro_col">
                <div class="intro_image magic_up">
                        <video width="100%" height="100%" controls style="background:#000;">
                                <source src="{{ URL::asset('front-theme/video/Jiangnan1.mp4')}}" type="video/mp4">
                                {{-- <source src="movie.ogg" type="video/ogg"> --}}
                                Your browser does not support the video tag.
                            </video>

                    <ul id="image-gallery" class="gallery list-unstyled cS-hidden" style="margin-top:20px;">
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/JiangnanTeaRoom/1.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/JiangnanTeaRoom/1.jpg')}}" />
                             </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/JiangnanTeaRoom/2.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/JiangnanTeaRoom/2.jpg')}}" />
                             </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/JiangnanTeaRoom/3.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/JiangnanTeaRoom/3.jpg')}}" />
                             </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/JiangnanTeaRoom/4.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/JiangnanTeaRoom/4.jpg')}}" />
                             </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/JiangnanTeaRoom/5.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/JiangnanTeaRoom/5.jpg')}}" />
                                </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/JiangnanTeaRoom/6.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/JiangnanTeaRoom/6.jpg')}}" />
                                </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/JiangnanTeaRoom/7.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/JiangnanTeaRoom/7.jpg')}}" />
                                </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/JiangnanTeaRoom/8.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/JiangnanTeaRoom/8.jpg')}}" />
                                </li>

                    </ul>


                </div>
            </div>

        </div>
    </div>
</div>


@endsection

@section('script')
<script src="{{ URL::asset('front-theme/js/about.js')}}"></script>
<script src="{{ URL::asset('front-theme/js/lightslider.js')}}"></script>
<script>
        $(document).ready(function() {
           $("#content-slider").lightSlider({
               loop:true,
               keyPress:true
           });
           $('#image-gallery').lightSlider({
               gallery:true,
               item:1,
               thumbItem:4,
               slideMargin: 0,
               speed:1500,
               auto:true,
               loop:true,
               onSliderLoad: function() {
                   $('#image-gallery').removeClass('cS-hidden');
               }
           });
       });
   </script>
@endsection
