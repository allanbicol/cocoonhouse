@extends('front.page-template')

@section('title')
<title>The Formal Lounge - Cocoon House</title>
@endsection

@section('meta')
<meta name="description" content="The formal lounge sits East of the atrium in magnificent contrast to the light and casual dining room. In winter months a large open fire warms the space, but the room appears to glow all year round, basking in the golden hues from the spectacular McCahon that graces the far wall. ">
<link rel="canonical" href="{{route('salon3')}}">
<meta property="og:type" content="article">
<meta property="og:title" content="The Formal Lounge - Cocoon House">
<meta property="og:description" content="The formal lounge sits East of the atrium in magnificent contrast to the light and casual dining room. In winter months a large open fire warms the space, but the room appears to glow all year round, basking in the golden hues from the spectacular McCahon that graces the far wall. ">
<meta property="og:image" content="{{ URL::asset('front-theme/images/the-formal-lounge.jpg')}}">
<meta property="og:url" content="{{route('salon3')}}">
<meta property="og:site_name" content="Cocoon House">

<meta name="twitter:title" content="The Formal Lounge - Cocoon House">
<meta name="twitter:description" content="The formal lounge sits East of the atrium in magnificent contrast to the light and casual dining room. In winter months a large open fire warms the space, but the room appears to glow all year round, basking in the golden hues from the spectacular McCahon that graces the far wall. ">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/the-formal-lounge.jpg')}}">
<meta name="twitter:card" content="summary_large_image">
@endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about_responsive.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/lightslider.css')}}">
@endsection

@section('breadcrumb')
<div class="home">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{ URL::asset('front-theme/images/home_slider_2.jpg')}}" data-speed="0.8"></div>
		<div class="home_content">
			<div class="home_subtitle">Cocoon House</div>
			<div class="home_title">Room Details</div>
		</div>
    </div>
</div>
@endsection

@section('content')
<div class="intro">
    <div class="container">
        <div class="row">

            <!-- Intro Content -->
            <div class="col-lg-6 intro_col">
                <div class="intro_container1 d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">
                        <div class="intro_text1">
                            <h3>Formal Lounge</h3>
                            <h5 style="color:#a8894c">Lounge Boardroom Exclusively</h5>
                            <div style="color:#000;font-weight:bold;padding-bottom:20px">8 hour hire fee  -  $ 1,000+gst<br>
                            half day prior to 3pm  -  $ 600+gst
                            </div>
                            <div class="col-lg-12">
                                <div class="row details-box">
                                    <table class="table">
                                        <tr>
                                            <td class="text-left"><i class="fa fa-cutlery"></i> &nbsp;Dining</td>
                                            <td class="text-right">16 guests</td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><i class="fa fa-briefcase"></i> &nbsp;Boardroom</td>
                                            <td class="text-right">16 guests</td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><i class="fa fa-glass"></i> &nbsp;Cocktail</td>
                                            <td class="text-right">25 guests</td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><i class="fa fa-ticket"></i> &nbsp;Theatre</td>
                                            <td class="text-right">20 guests</td>
                                        </tr>
                                    </table>

                                </div>
                            </div>
                            <div style="height: 50px;border-top:1px solid #efefef;"></div>
                            <p>
                                    The formal lounge sits East of the atrium in magnificent contrast to the light and casual dining room.
                                    In winter months a large open fire warms the space, but the room appears to glow all year round, basking in the golden hues from the spectacular McCahon that graces the far wall.
                                    The lounge is a perfect space for board meetings and is easily converted with the inclusion of our custom-built boardroom table, seating 14 guests.
                                    All AV requirements are available and facilities for video conferencing can be booked.
                            </p>


                        </div>

                    </div>
                </div>
            </div>

            <!-- Intro Image -->
            <div class="col-lg-6 intro_col">
                <div class="intro_image magic_up">
                    <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/FormalLounge/1.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/FormalLounge/1.jpg')}}" />
                             </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/FormalLounge/2.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/FormalLounge/2.jpg')}}" />
                             </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/FormalLounge/3.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/FormalLounge/3.jpg')}}" />
                             </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/FormalLounge/4.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/FormalLounge/4.jpg')}}" />
                             </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/FormalLounge/5.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/FormalLounge/5.jpg')}}" />
                                </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/FormalLounge/6.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/FormalLounge/6.jpg')}}" />
                                </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/FormalLounge/7.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/FormalLounge/7.jpg')}}" />
                                </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/FormalLounge/8.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/FormalLounge/8.jpg')}}" />
                                </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/FormalLounge/9.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/FormalLounge/9.jpg')}}" />
                                </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/FormalLounge/10.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/FormalLounge/10.jpg')}}" />
                                </li>
                        <li data-thumb="{{ URL::asset('front-theme/images/spaces/FormalLounge/11.jpg')}}">
                            <img src="{{ URL::asset('front-theme/images/spaces/FormalLounge/11.jpg')}}" />
                                </li>

                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>


@endsection

@section('script')
<script src="{{ URL::asset('front-theme/js/about.js')}}"></script>
<script src="{{ URL::asset('front-theme/js/lightslider.js')}}"></script>
<script>
        $(document).ready(function() {
           $("#content-slider").lightSlider({
               loop:true,
               keyPress:true
           });
           $('#image-gallery').lightSlider({
               gallery:true,
               item:1,
               thumbItem:4,
               slideMargin: 0,
               speed:1500,
               auto:true,
               loop:true,
               onSliderLoad: function() {
                   $('#image-gallery').removeClass('cS-hidden');
               }
           });
       });
   </script>
@endsection
