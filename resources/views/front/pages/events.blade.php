@extends('front.page-template')

@section('title')
<title>Cocoon Curated Events - Cocoon House</title>
@endsection

@section('meta')
<meta name="description" content="Our preferred caterers will quote you the full event experience including
food, bar service, staff, crockery, cutlery and glassware as required.">
<link rel="canonical" href="{{route('story')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="Cocoon Curated Events - Cocoon House">
<meta property="og:description" content="Our preferred caterers will quote you the full event experience including
food, bar service, staff, crockery, cutlery and glassware as required.">
<meta property="og:image" content="{{ URL::asset('front-theme/images/rosa.jpg')}}">
<meta property="og:url" content="{{route('story')}}">
<meta property="og:site_name" content="Cocoon House">

<meta name="twitter:title" content="Cocoon Curated Events - Cocoon House">
<meta name="twitter:description" content="Our preferred caterers will quote you the full event experience including
food, bar service, staff, crockery, cutlery and glassware as required.">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/rosa.jpg')}}">
<meta name="twitter:card" content="summary_large_image">
@endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/rooms.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/rooms_responsive.css')}}">
@endsection

@section('breadcrumb')
<div class="home">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{ URL::asset('front-theme/images/home_slider_2.jpg')}}" data-speed="0.8"></div>
		<div class="home_content">
			<div class="home_subtitle">Cocoon House</div>
			<div class="home_title">Cocoon Curated Events</div>
		</div>
    </div>
</div>
@endsection

@section('content')
{{-- <div class="intro">
    <div class="container">
        <div class="row" style="padding-bottom:100px;">
            <div class="col-lg-12 text-center">
                <video width="70%" height="100%" controls style="background:#000;">
                    <source src="{{ URL::asset('front-theme/video/CocoonWineShowcase1.mp4')}}" type="video/mp4">
                    Your browser does not support the video tag.
                </video>

                <h3 style="margin-top:50px;">Upcoming event details coming soon.</h3>
            </div>
        </div>
    </div>
</div> --}}


<div class="container">
    <div class="row" style="margin-bottom:100px;margin-top:50px;">

        <div class="col-lg-12">
            <div id="ect-events-list-content" class="ectt-list-wrapper">
                @if ($agent->isMobile())
                    @foreach ($events as $event)
                    <div class="card card-body" style="background:#e6dcc9;">
                        <div class="row" >
                            <div class="col-md-4 col-lg-3 text-center">
                                <a href="{{route('rwc-event',['id'=> $event->id])}}"><img src="{{ URL::asset('front-theme/images/events')}}/{{$event->image}}" alt="user" style="width:100%"></a>
                            </div>
                            <div class="col-md-8 col-lg-9">
                                <h3 class="box-title m-b-0"><a style="color:#333;" href="{{route('rwc-event',['id'=> $event->id])}}" alt="{{$event->event_sub_title}}" rel="bookmark">{{$event->event_title}}</a></h3>
                                <h6>{{$event->event_sub_title}}</h6>
                                <div style="font-size:25px;color:#a8894c;">POSTPONED</div>
                                {{-- <div style="font-size:20px;color:#a8894c;">{{ \Carbon\Carbon::parse($event->event_date)->format('F d')}} 6.30pm - 8.30pm</div> --}}

                                <div class="ect-list-cost">
                                    <b>${{$event->price}}</b> inc. GST per person
                                </div>
                                <div class="ect-event-content" itemprop="description" content="Chandigarh it’s time to wake up your taste buds with The Grub Fest, India’s biggest and most celebrated food festival with the best restaurants displaying their signature delicacies. Note:- It's not original event.">
                                    {{-- <p>{!!$event->details!!}</p> --}}
                                    <p style="color:red;">Gin doesn't normally make us emotional, but this announcement does 😢 We regret to advise that this event has been cancelled - we can't wait to reschedule and let you know the date for our next Masterclass.</p>
                                    <p>{{$event->about}}</p>
                                    <br>
                                    <p style="color:red;">Note : Limited street parking available</p>
                                    <br>
                                    <a href="{{route('rwc-event',['id'=> $event->id])}}" class="ect-events-read-more" rel="bookmark">More Info »</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @else
                    @foreach ($events as $event)
                    <div id="list-wrp" class="ect-list-wrapper all">
                            <div id="event-15" class="ect-list-post style-1 ect-simple-event" itemscope="" itemtype="http://schema.org/Event">
                                <div class="ect-list-post-left ">
                                    <a href="{{route('rwc-event',['id'=> $event->id])}}" alt="{{$event->event_sub_title}}" rel="bookmark">
                                        <div class="ect-list-img" style="background-image:url('{{ URL::asset('front-theme/images/events')}}/{{$event->image}}');background-size:cover;">
                                        </div>
                                    </a>
                                </div>

                                <div class="ect-list-post-right">
                                    <div class="ect-list-post-right-table">
                                        <div class="ect-list-description">
                                            <h3>
                                                <a itemprop="name" class="ect-event-url" href="{{route('rwc-event',['id'=> $event->id])}}" rel="bookmark">{{$event->event_title}}</a>
                                            </h3>
                                            <h6>{{$event->event_sub_title}}</h6>
                                            <div class="ect-list-cost">
                                                <b>${{$event->price}}</b> inc. GST per person
                                            </div>
                                            <div class="ect-event-content" itemprop="description" content="Chandigarh it’s time to wake up your taste buds with The Grub Fest, India’s biggest and most celebrated food festival with the best restaurants displaying their signature delicacies. Note:- It's not original event.">
                                                {{-- <p>{!!$event->details!!}</p> --}}
                                                <p style="color:red;">Gin doesn't normally make us emotional, but this announcement does 😢 We regret to advise that this event has been cancelled - we can't wait to reschedule and let you know the date for our next Masterclass.</p>
                                                <p>{{$event->about}}</p>
                                                <br>
                                                <br>
                                                <p style="color:red;">Note : Limited street parking available</p>
                                                <br>
                                                <br>
                                                <a href="{{route('rwc-event',['id'=> $event->id])}}" class="ect-events-read-more" rel="bookmark">More Info »</a>
                                            </div>

                                        </div>
                                        <div class="ect-list-venue default-venue" style="vertical-align:middle;">
                                                {{-- <div style="font-size:40px;">{{ \Carbon\Carbon::parse($event->event_date)->format('d')}}</div>
                                                <div>{{ \Carbon\Carbon::parse($event->event_date)->format('F')}}</div>
                                                <br>
                                                <div>6.30pm - 8.30pm</div> --}}
                                                <div style="font-size:25px;color:#fff;">POSTPONED</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif

            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="{{ URL::asset('front-theme/js/rooms.js')}}"></script>

@endsection
