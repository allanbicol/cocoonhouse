@extends('front.page-template')

@section('title')
<title>Rosa's Story - Cocoon House</title>
@endsection

@section('meta')
<meta name="description" content="Rosa Tan is an art collector and lover of all things beautifully crafted, from furniture design to wine, and her personal favourite… tea. In 2016 a beautiful apartment at 35 Spring Street was advertised for sale. From the moment Rosa stepped in to the apartment she knew it would be called Cocoon. A retreat from the outside world, and a place of transformation. The Cocoon would be wrapped in beautiful art and spectacular treasures, but it would feel like a home rather than a gallery.">
<link rel="canonical" href="{{route('story')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="Rosa's Story - Cocoon House">
<meta property="og:description" content="Rosa Tan is an art collector and lover of all things beautifully crafted, from furniture design to wine, and her personal favourite… tea. In 2016 a beautiful apartment at 35 Spring Street was advertised for sale. From the moment Rosa stepped in to the apartment she knew it would be called Cocoon. A retreat from the outside world, and a place of transformation. The Cocoon would be wrapped in beautiful art and spectacular treasures, but it would feel like a home rather than a gallery.">
<meta property="og:image" content="{{ URL::asset('front-theme/images/rosa.jpg')}}">
<meta property="og:url" content="{{route('story')}}">
<meta property="og:site_name" content="Cocoon House">

<meta name="twitter:title" content="Rosa's Story - Cocoon House">
<meta name="twitter:description" content="Rosa Tan is an art collector and lover of all things beautifully crafted, from furniture design to wine, and her personal favourite… tea. In 2016 a beautiful apartment at 35 Spring Street was advertised for sale. From the moment Rosa stepped in to the apartment she knew it would be called Cocoon. A retreat from the outside world, and a place of transformation. The Cocoon would be wrapped in beautiful art and spectacular treasures, but it would feel like a home rather than a gallery.">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/rosa.jpg')}}">
<meta name="twitter:card" content="summary_large_image">
@endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about_responsive.css')}}">
@endsection

@section('breadcrumb')
<div class="home">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{ URL::asset('front-theme/images/home_slider_2.jpg')}}" data-speed="0.8"></div>
		<div class="home_content">
			<div class="home_subtitle">Cocoon House</div>
			<div class="home_title">Rosa's Story</div>
		</div>
    </div>
</div>
@endsection

@section('content')
<div class="intro">
    <div class="container">
        <div class="row">

            <!-- Intro Content -->
            <div class="col-lg-6 intro_col">
                <div class="intro_container1 d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">
                        <div class="intro_text1">
                            <p style="margin-top:10px;">
                                    Rosa Tan is an art collector and lover of all things beautifully crafted, from furniture design to wine, and her personal favourite… tea.
                                    In 2016 a beautiful apartment at 35 Spring Street was advertised for sale.
                                    From the moment Rosa stepped in to the apartment she knew it would be called Cocoon.
                                     A retreat from the outside world, and a place of transformation.
                                     The Cocoon would be wrapped in beautiful art and spectacular treasures, but it would feel like a home rather than a gallery.
                                     A place where conversation and laughter tumble out of every room, filling the central courtyard like a swimming pool of delight.
                            </p>
                            <p>
                                    Rosa’s life began with a total absence of art and individual expression.
                                    As a child of the cultural revolution she would wear the same grey green uniform as her parents and grandparents,
                                    everyone she knew looked exactly the same. Mao was in favour of collective identity and expressive clothes were unacceptable during his regime.
                                    Rosa remembers with a sparkle that her mother secretly made two beautiful dresses for her, embroidered with flowers and animals,
                                    resplendent with colour and joy. But these treasures could only be worn at home and shown to nobody.
                            </p>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Intro Image -->
            <div class="col-lg-6 intro_col">
                    <div class="intro_image magic_up">
                        <img src="{{ URL::asset('front-theme/images/rosa1.jpg')}}" width="100%" style="padding:15px;margin-top:0px;" alt="">
                    </div>
                </div>

            <div class="col-lg-12">

                <p style="margin-top:10px;">
                        Decades later, there is nothing Rosa loves more than sharing the beautiful things of her life.
                        The art collection at Cocoon House includes works from Picasso and other international masters,
                        but at its heart the collection is about New Zealand. Aotearoa through the eyes of McCahon, Parekowhai, Frizzell, Walsh and Gimblett.
                </p>
                <p>
                        Moving to New Zealand at 26 was the start of a new life and new young family for Rosa.
                        Her love of art evolved hand in hand with her love of New Zealand, and consequently the two are forever linked.
                        But as an adult and free of Mao’s cultural suppression Rosa also discovered the aesthetic treasures of her Chinese roots,
                        finding special affection for the Ming and Song Dynasties.
                        Many of the commissioned pieces of furniture in Cocoon House are inspired by designs from this period.
                        Rosa’s greatest love from Chinese culture has become a signature of the house. The tea ceremony is an art form for Rosa, and a daily joy.
                        Often referring to it as tea yoga, Rosa is always happy to share the ritual with friends and new acquaintances.
                        Walking past the McCahon or Parekowhai on your way to sharing the tea ceremony with Rosa is a sublime meeting of cultures and the perfect experience of Cocoon House.
                </p>
                <p>
                        The Cocoon Salon of Art, Wine and Design is a group of eclectic friends drawn together by a shared love of the arts and creativity.
                        Cocoon House is home to the Salon and sometimes exhibits pieces or collections from Salon members.
                        Which means you never quite know what new treats might be on the walls when you visit.
                </p>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')
<script src="{{ URL::asset('front-theme/js/about.js')}}"></script>
@endsection
