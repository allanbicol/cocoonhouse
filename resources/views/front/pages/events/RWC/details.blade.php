@extends('front.page-template')

@section('title')
<title>Cocoon Curated Events - Cocoon House</title>
@endsection

@section('meta')
    <meta name="description" content="Our preferred caterers will quote you the full event experience including
    food, bar service, staff, crockery, cutlery and glassware as required.">
    <link rel="canonical" href="{{route('story')}}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Cocoon Curated Events - Cocoon House">
    <meta property="og:description" content="Our preferred caterers will quote you the full event experience including
    food, bar service, staff, crockery, cutlery and glassware as required.">
    <meta property="og:image" content="{{ URL::asset('front-theme/images/rosa.jpg')}}">
    <meta property="og:url" content="{{route('story')}}">
    <meta property="og:site_name" content="Cocoon House">

    <meta name="twitter:title" content="Cocoon Curated Events - Cocoon House">
    <meta name="twitter:description" content="Our preferred caterers will quote you the full event experience including
    food, bar service, staff, crockery, cutlery and glassware as required.">
    <meta name="twitter:image" content="{{ URL::asset('front-theme/images/rosa.jpg')}}">
    <meta name="twitter:card" content="summary_large_image">
@endsection

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about_responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/lightslider.css')}}">
@endsection

@section('breadcrumb')
<div class="home">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{ URL::asset('front-theme/images/home_slider_2.jpg')}}" data-speed="0.8"></div>
		<div class="home_content">
			<div class="home_subtitle">Cocoon House</div>
            <div class="home_title">{{$event->event_title}}</div>
		</div>
    </div>
</div>
@endsection

@section('content')
<div class="intro">
    <div class="container">
        <div class="row">

            <!-- Intro Content -->
            <div class="col-lg-7 intro_col">
                <div class="intro_container1 d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">
                        <div class="intro_text1">
                            <h3>{{$event->event_title}}</h3>
                            {{-- <div style="font-size:25px;color:#a8894c;">{{ \Carbon\Carbon::parse($event->event_date)->format('d')}} {{ \Carbon\Carbon::parse($event->event_date)->format('F')}}</div> --}}
                            <div style="font-size:25px;color:#a8894c;">POSTPONED</div>
                            <div style="color:#333;font-size:20px">${{$event->price}} + gst per person</div>
                            <div style="height: 50px;border-top:1px solid #efefef;"></div>

                            <table class="table" style="color:#a8894c;">

                                <tbody>
                                  <tr>
                                    <th scope="row" style="width:30%">Subject</th>
                                    <td>Consider yourself a GIN connoisseur..?</td>

                                  </tr>
                                  <tr>
                                    <th scope="row">Date</th>
                                    <td>Wednesday, 8th April 2020</td>

                                  </tr>
                                  <tr>
                                    <th scope="row">Time</th>
                                    <td>6.30pm - 8.30pm</td>

                                  </tr>
                                  <tr>
                                    <th scope="row">Dress Code</th>
                                    <td>Smart casual</td>

                                  </tr>
                                  <tr>
                                    <th scope="row">Please note</th>
                                    <td>Limited street parking available</td>

                                  </tr>
                                  <tr>
                                    <th scope="row">Register</th>
                                    <td>
                                        PURCHASE YOUR TICKETS FOR COCOON'S GIN MASTERCLASS<br>
                            <br>Confirm your tickets with <a href="mailto:joe.wang@cocoonhouse.co.nz">Joe Wang</a>
                                    </td>

                                  </tr>
                                </tbody>
                              </table>
                            <p style="color:red;">Gin doesn't normally make us emotional, but this announcement does 😢 We regret to advise that this event has been cancelled - we can't wait to reschedule and let you know the date for our next Masterclass.</p>
                            <p>
                                    {!!$event->details!!}
                            </p>


                            <div class="blank-space"></div>
                            {{-- <div style="padding:10px;background-color:#e8e2d6;">
                                <h6>Complete this form to register</h6>
                                @if($message = Session::get('success'))
                                    <div class="alert alert-icon alert-success border-success alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                        </button><strong>Well done!</strong> {{ $message }}.
                                    </div>
                                @endif
                                <form method="POST" action="{{ route('rwc-event-register')}}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="event" value="{{$event->event_sub_title}}">
                                    <input type="hidden" name="image" value="{{$event->image}}">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <input type="text" name="fname" class="newsletter_input" placeholder="First Name" required="required">
                                        </div>
                                        <div class="col-lg-6">
                                            <input type="text" name="lname" class="newsletter_input" placeholder="Last Name" required="required">
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top:20px">
                                        <div class="col-lg-6">
                                            <input type="email" name="email" class="newsletter_input" placeholder="Email Address" required="required">
                                        </div>
                                        <div class="col-lg-6">
                                            <input type="text" name="phone" class="newsletter_input" placeholder="Phone Number" required="required">
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top:20px">
                                        <div class="col-lg-6">
                                            <input type="text" name="company" class="newsletter_input" placeholder="Company">
                                        </div>
                                        <div class="col-lg-6">
                                            <input type="number" name="number_of_attendees" class="newsletter_input" placeholder="Number of Attendees" required="required">
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top:20px">
                                            <div class="col-lg-6">
                                                    <input type="submit" class="button button_container room_button " style="color:#fff" value="Register" />
                                            </div>
                                        </div>
                                </form>
                            </div> --}}
                        </div>

                    </div>
                </div>
            </div>

            <!-- Intro Image -->
            <div class="col-lg-5 intro_col">
                <div class="intro_image magic_up">
                    <img src="{{ URL::asset('front-theme/images/events')}}/{{$event->image}}" width="100%" alt="">

                </div>
            </div>

            <div class="col-lg-12">
                <h5>More Details</h5>
                <p>Puhoi Organic Distillery has become a boutique niche brand recognised internationally created by Kirichuk family, and based solely on the internal family’s knowledge, skills and resources.</p>
                <p>Despite the miniature size of the Distillery team, operated by only three family members, the Distillery’s range amazes the connoisseurs both with the number of the products, and with the unobtainable level of their bespoke quality.</p>
                <p>“We apply the curative power of Mother Nature to bring happiness and wellbeing to people and leave only green footprints on the Planet” reflects the family’s business philosophy and the revolutionary technological concept invented by the family.</p>
                <p>Alex Kirichuk is a celebrated Master Distiller, who graduated as a Nuclear Power Engineer in the USSR and then re-applied his unique set of skills and knowledge to the invention of revolutionising, cutting edge eco-friendly distillation technologies and equipment here in NZ. Alex has been recognised as a guru in the art of distillation internationally, and his advice has been sought by the professionals from around the globe.</p>
                <p>Alex, basically received a death sentence after the Chernobyl disaster in '86, due to the cruel communist system of the USSR which deployed Alex and his colleagues to deal with the nuclear power incident. However, his wife Dr Iryna Kirichuk (MD), had different plans, and used her knowledge to save the life of the future Master Distiller.</p>
                <p>Dr Iryna Kirichuk (MD), presently is the world’s only expert in Enotherapy practicing in New Zealand.  She transforms more than 30 years of her medical experience (specialising in Cardiology) into the unique formulations of functional beverages according to the Science of Enotherapy, which are handcrafted by Puhoi Organic Distillery. Dr Kirichuk (MD) is also one of the top international experts in the fields of Natural Adaptogens, herbal medicine, bee products and other natural methods of treatment.</p>
            </div>

        </div>
    </div>
</div>


@endsection

@section('script')
<script src="{{ URL::asset('front-theme/js/about.js')}}"></script>

@endsection
