@extends('front.page-template')

@section('title')
<title>Find - Cocoon House</title>
@endsection

@section('meta')
<meta name="description" content="We are based in Freemans Bay, on the periphery of the CBD, a short walk to Victoria Park and Wynyard Quarter. There is no designated parking at the venue.">
<link rel="canonical" href="{{route('find')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="Find - Cocoon House">
<meta property="og:description" content="We are based in Freemans Bay, on the periphery of the CBD, a short walk to Victoria Park and Wynyard Quarter. There is no designated parking at the venue.">
<meta property="og:image" content="{{ URL::asset('front-theme/images/home_slider_2.jpg')}}">
<meta property="og:url" content="{{route('find')}}">
<meta property="og:site_name" content="Cocoon House">

<meta name="twitter:title" content="Find - Cocoon House">
<meta name="twitter:description" content="We are based in Freemans Bay, on the periphery of the CBD, a short walk to Victoria Park and Wynyard Quarter. There is no designated parking at the venue.">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/home_slider_2.jpg')}}">
<meta name="twitter:card" content="summary_large_image">
@endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about_responsive.css')}}">
@endsection

@section('breadcrumb')
<div class="home">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{ URL::asset('front-theme/images/home_slider_2.jpg')}}" data-speed="0.8"></div>
		<div class="home_content">
			<div class="home_subtitle">Cocoon House</div>
			<div class="home_title">Find Cocoon</div>
		</div>
    </div>
</div>
@endsection

@section('content')
<div class="intro">
    <div class="container">
        <div class="row">

            <!-- Intro Content -->
            <div class="col-lg-12 intro_col">
                <div class="intro_container1 d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">
                        <div class="intro_text1">
                            <h5>DIRECTIONS</h5>
                            <p>
                                    We are based in Freemans Bay, on the periphery of the CBD, a short walk to Victoria Park and Wynyard Quarter. Please note, there is no designated parking at the venue and limited street parking available.
                            </p>

                        </div>

                    </div>
                </div>
            </div>


            <div class="col-lg-12" style="padding-top:40px;">
                    <iframe frameborder="0" style="width:100%;min-height:400px" src="https://www.google.com/maps?q=35%20Spring%20St%2C%20Freemans%20Bay%2C%20Auckland%201011%2C%20New%20Zealand&amp;output=embed&amp;hl=%s&amp;z=14"></iframe>
            </div>

            <div class="col-lg-12" style="padding-top:100px;">
                    <h5>TERMS & CONDITIONS</h5>
                    <p>
                            Spring Street is a mixed-use area, we are cognisant and considerate of our residential neighbours – for that reason, we are not available for parties requiring live bands and loud music.
                    </p>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')
<script src="{{ URL::asset('front-theme/js/about.js')}}"></script>

@endsection
