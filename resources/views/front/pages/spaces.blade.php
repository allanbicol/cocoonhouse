@extends('front.page-template')

@section('title')
<title>Spaces - Cocoon House</title>
@endsection

@section('meta')
<meta name="description" content="The Salon Floor comprises the Dining Room and Formal Lounge connected by the Atrium space. The top level offers many gorgeous views to the courtyard below and three diverse spaces. In true Cocoon fashion one room reflects European culture, one Asian and one features taonga totally unique to Aotearoa. The lavish but ever so intimate Cocoon Bar is a hidden treasure of the house.">
<link rel="canonical" href="{{route('spaces')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="Spaces - Cocoon House">
<meta property="og:description" content="The Salon Floor comprises the Dining Room and Formal Lounge connected by the Atrium space. The top level offers many gorgeous views to the courtyard below and three diverse spaces. In true Cocoon fashion one room reflects European culture, one Asian and one features taonga totally unique to Aotearoa. The lavish but ever so intimate Cocoon Bar is a hidden treasure of the house.">
<meta property="og:image" content="{{ URL::asset('front-theme/images/salon_2.jpg')}}">
<meta property="og:url" content="{{route('spaces')}}">
<meta property="og:site_name" content="Cocoon House">

<meta name="twitter:title" content="Spaces - Cocoon House">
<meta name="twitter:description" content="The Salon Floor comprises the Dining Room and Formal Lounge connected by the Atrium space. The top level offers many gorgeous views to the courtyard below and three diverse spaces. In true Cocoon fashion one room reflects European culture, one Asian and one features taonga totally unique to Aotearoa. The lavish but ever so intimate Cocoon Bar is a hidden treasure of the house.">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/salon_2.jpg')}}">
<meta name="twitter:card" content="summary_large_image">
@endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/rooms.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/rooms_responsive.css')}}">
@endsection

@section('breadcrumb')
<div class="home">
    <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{ URL::asset('front-theme/images/home_slider_2.jpg')}}" data-speed="0.8"></div>
        <div class="home_content">
            <div class="home_subtitle">Cocoon House</div>
            <div class="home_title">Spaces</div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="rooms">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="section_title_container text-left">
                    {{-- <div class="section_subtitle">cocoon house</div> --}}
                    <div class="section_title"><h2>Salon Floor</h2></div>
                    <p>The Cocoon Salon is situated on the first floor with each room opening to the central atrium. Inspired by the famous Hutong courtyards of ancient Beijing, this courtyard and lush Mexican Elder tree are the centrepiece of the property and visible from nearly every room.</p>
                    <p>The Salon Floor encompasses the Dining Room and Formal Lounge, connected by the Atrium space.</p>

                </div>
            </div>
        </div>
        <div class="row room_row">

            <!-- Room -->
            <div class="col-lg-4 room_col ">
                <div class="room">
                    <div class="room_image"><a href="{{ route('salon1') }}"><div class="zoom"><img src="{{ URL::asset('front-theme/images/salon_1.jpg')}}"  alt="THE DINING ROOM"></div></a></div>
                    <div class="room_content text-center">
                        <div class="room_title"><a href="{{ route('salon1') }}">DINING ROOM</a></div>
                        {{-- <div class="room_text">
                            <table class="table">
                                <tr>
                                    <td class="text-left"><i class="fa fa-cutlery"></i> &nbsp;Dining</td>
                                    <td class="text-right">30 pax</td>
                                </tr>
                                <tr>
                                    <td class="text-left"><i class="fa fa-briefcase"></i> &nbsp;Boardroom</td>
                                    <td class="text-right">18 pax</td>
                                </tr>
                                <tr>
                                    <td class="text-left"><i class="fa fa-glass"></i> &nbsp;Cocktail</td>
                                    <td class="text-right">40 pax</td>
                                </tr>
                                <tr>
                                    <td class="text-left"></td>
                                    <td class="text-right"></td>
                                </tr>
                            </table>
                        </div> --}}
                        {{-- <a href="#" class="button_container room_button"><div class="button text-center"><span>Book Now</span></div></a> --}}
                    </div>
                </div>
            </div>

            <!-- Room -->
            <div class="col-lg-4 room_col " >
                <div class="room">
                    <div class="room_image"><a href="{{route('salon2')}}"><div class="zoom"><img src="{{ URL::asset('front-theme/images/salon_2.jpg')}}" alt="THE ATRIUM"></div></a></div>
                    <div class="room_content text-center">
                        <div class="room_title"><a href="{{route('salon2')}}">ATRIUM</a></div>
                        {{-- <div class="room_text">
                            <table class="table">
                                <tr>
                                    <td class="text-left"><i class="fa fa-cutlery"></i> &nbsp;Dining</td>
                                    <td class="text-right">20 pax</td>
                                </tr>
                                <tr>
                                    <td class="text-left"><i class="fa fa-glass"></i> &nbsp;Cocktail</td>
                                    <td class="text-right">30 pax</td>
                                </tr>
                                <tr>
                                    <td class="text-left"><i class="fa fa-ticket"></i> &nbsp;Theatre</td>
                                    <td class="text-right">20 pax</td>
                                </tr>

                                <tr>
                                    <td class="text-left"></td>
                                    <td class="text-right"></td>
                                </tr>
                            </table>
                        </div> --}}
                        {{-- <a href="#" class="button_container room_button"><div class="button text-center"><span>Book Now</span></div></a> --}}
                    </div>
                </div>
            </div>

            <!-- Room -->
            <div class="col-lg-4 room_col">
                <div class="room">
                    <div class="room_image "><a href="{{route('salon3')}}"><div class="zoom"><img src="{{ URL::asset('front-theme/images/salon_3.jpg')}}" alt="THE FORMAL LOUNGE"></div></a></div>
                    <div class="room_content text-center">
                        <div class="room_title"><a href="{{route('salon3')}}">FORMAL LOUNGE</a></div>
                        {{-- <div class="room_text">
                            <table class="table">
                                <tr>
                                    <td class="text-left"><i class="fa fa-cutlery"></i> &nbsp;Dining</td>
                                    <td class="text-right">16 pax</td>
                                </tr>
                                <tr>
                                    <td class="text-left"><i class="fa fa-briefcase"></i> &nbsp;Boardroom</td>
                                    <td class="text-right">16 pax</td>
                                </tr>
                                <tr>
                                    <td class="text-left"><i class="fa fa-glass"></i> &nbsp;Cocktail</td>
                                    <td class="text-right">25 pax</td>
                                </tr>
                                <tr>
                                    <td class="text-left"><i class="fa fa-ticket"></i> &nbsp;Theatre</td>
                                    <td class="text-right">20 pax</td>
                                </tr>
                            </table>
                        </div> --}}
                        {{-- <a href="#" class="button_container room_button"><div class="button text-center"><span>Book Now</span></div></a> --}}
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-6">
                        <h5>8 hour hire fee | $4,000+gst</h5>
                        <h5>half day prior to 3pm | $2,500+gst</h5>
                        {{-- <h5>Half day – prior to 3.00pm | $2,500+gst</h5> --}}
                    </div>
                    <div class="col-lg-6 text-right">
                        <a href="mailto:info@cocoonhouse.co.nz?subject=Cocoon House - Salon Floor Booking" class="button_container"><div class="button text-center"><span>Book Now</span></div></a>
                    </div>
                </div>


            </div>


        </div>
    </div>
</div>

<div class="rooms1">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="section_title_container text-left">
                    {{-- <div class="section_subtitle">cocoon house</div> --}}
                    <div class="section_title"><h2>TOP FLOOR</h2></div>
                    <p>The top level offers many gorgeous views to the courtyard below and three diverse spaces. In true Cocoon fashion one room reflects European culture, one Asian and one features taonga totally unique to Aotearoa.</p>

                </div>
            </div>
        </div>
        <div class="row room_row">

            <!-- Room -->
            <div class="col-lg-4 room_col">
                <div class="room">
                    <div class="room_image"><a href="{{route('top1')}}"><div class="zoom"><img src="{{ URL::asset('front-theme/images/top1.jpg')}}" alt="JIANGNAN TEA ROOM"></div></a></div>
                    <div class="room_content text-center">
                        {{-- <div class="room_price">$800.00 + gst</div> --}}
                        <div class="room_title"><a href="{{route('top1')}}">JIANGNAN TEA ROOM</a></div>
                        {{-- <div class="room_text">
                            <table class="table">
                                <tr>
                                    <td class="text-left"><i class="fa fa-money"></i> &nbsp;Price</td>
                                    <td class="text-right">$800.00 + gst<br><div style="font-size:8px;color:#000;">+ tea selections (inc. Tea Master)</td>
                                </tr>
                                <tr>
                                    <td class="text-left"><i class="fa fa-user"></i> &nbsp;Seated</td>
                                    <td class="text-right">8 pax</td>
                                </tr>
                            </table>
                        </div>
                        <a href="mailto:info@cocoonhouse.co.nz?subject=Cocoon House - Jiangnan Tea Room Booking" class="button_container room_button"><div class="button text-center"><span>Book Now</span></div></a> --}}
                    </div>
                </div>
            </div>

            <!-- Room -->
            <div class="col-lg-4 room_col">
                <div class="room">
                    <div class="room_image"><a href="{{route('top2')}}"><div class="zoom"><img src="{{ URL::asset('front-theme/images/top2.jpg')}}" alt="FORNASETTI LIBRARY"></div></a></div>
                    <div class="room_content text-center">
                        {{-- <div class="room_price"> $500.00 + gst </div> --}}
                        <div class="room_title"><a href="{{route('top2')}}">FORNASETTI LIBRARY</a></div>
                        {{-- <div class="room_text">
                            <table class="table">
                                <tr>
                                    <td class="text-left"><i class="fa fa-money"></i> &nbsp;Price</td>
                                    <td class="text-right">$500.00 + gst</td>
                                </tr>
                                <tr>
                                    <td class="text-left"><i class="fa fa-briefcase"></i> &nbsp;Meeting</td>
                                    <td class="text-right">6 pax</td>
                                </tr>
                                <tr>
                                    <td class="text-left"><i class="fa fa-glass"></i> &nbsp;Cocktail</td>
                                    <td class="text-right">8 pax</td>
                                </tr>
                            </table>
                        </div>
                        <a href="mailto:info@cocoonhouse.co.nz?subject=Cocoon House - Fornasetti Library Booking" class="button_container room_button"><div class="button text-center"><span>Book Now</span></div></a> --}}
                    </div>
                </div>
            </div>

            <!-- Room -->
            <div class="col-lg-4 room_col">
                <div class="room">
                    <div class="room_image"><a href="{{route('top3')}}"><div class="zoom"><img src="{{ URL::asset('front-theme/images/top3.jpg')}}" alt="BOARDROOM TU"></div></a></div>
                    <div class="room_content text-center">
                        {{-- <div class="room_price">500.00 + gst</div> --}}
                        <div class="room_title"><a href="{{route('top3')}}">BOARDROOM TU</a></div>
                        {{-- <div class="room_text">
                            <table class="table">
                                <tr>
                                    <td class="text-left"><i class="fa fa-money"></i> &nbsp;Price</td>
                                    <td class="text-right">$500.00 + gst</td>
                                </tr>
                                <tr>
                                    <td class="text-left"><i class="fa fa-cutlery"></i> &nbsp;Dining</td>
                                    <td class="text-right">8 pax</td>
                                </tr>
                                <tr>
                                    <td class="text-left"><i class="fa fa-briefcase"></i> &nbsp;Meeting</td>
                                    <td class="text-right">8 pax</td>
                                </tr>
                            </table>
                        </div>
                        <a href="mailto:info@cocoonhouse.co.nz?subject=Cocoon House - Boardroom Tu Booking" class="button_container room_button"><div class="button text-center"><span>Book Now</span></div></a> --}}
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>

<div class="rooms1">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="section_title_container text-left">
                    {{-- <div class="section_subtitle">cocoon house</div> --}}
                    <div class="section_title"><h2>GROUND FLOOR</h2></div>
                    <p>The lavish but ever so intimate Cocoon Bar is a hidden treasure of the house.</p>

                </div>
            </div>
        </div>
        <div class="row room_row">

            <!-- Room -->
            <div class="col-lg-4 room_col">
                <div class="room">
                    <div class="room_image"><a href="{{ route('ground1')}}"><div class="zoom"><img src="{{ URL::asset('front-theme/images/ground_1.jpg')}}" alt="COCOON BAR"></div></a></div>
                    <div class="room_content text-center">
                        {{-- <div class="room_price">$1,500.00 + gst</div> --}}
                        <div class="room_title"><a href="{{ route('ground1')}}">COCOON BAR</a></div>
                        {{-- <div class="room_text">
                            <table class="table">
                                <tr>
                                    <td class="text-left"><i class="fa fa-money"></i> &nbsp;Price</td>
                                    <td class="text-right">$1,500.00 + gst <div style="font-size:8px;color:#000;">(includes Mixologist)</td>
                                </tr>
                                <tr>
                                    <td class="text-left"><i class="fa fa-user"></i> &nbsp;Cocktail</td>
                                    <td class="text-right">20 pax</td>
                                </tr>
                            </table>
                        </div>
                        <a href="mailto:info@cocoonhouse.co.nz?subject=Cocoon House - Cocoon Bar Booking" class="button_container room_button"><div class="button text-center"><span>Book Now</span></div></a> --}}
                    </div>
                </div>
            </div>





        </div>
    </div>
</div>

<div class="milestones">
    <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{ URL::asset('front-theme/images/milestones.jpg')}}" data-speed="0.8"></div>
    <div class="milestones_background_overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="milestones_text">
                    <p>Full hire Cocoon House: $6,000 + gst </p>
                    <p>Hire includes: Post event cleaning, Utilities & Public Liability Insurance</p>
                </div>
            </div>
            <div class="col-lg-4 text-right">
                <a href="{{ URL::asset('front-theme/downloadables/Cocoon House - Venue Hire Pack 2020_new.pdf')}}" target="_blank" class="button_container"><div class="button text-center btn-download"><span>Download Venue Hire Pack</span></div></a>
            </div>
        </div>

    </div>

</div>


@endsection

@section('script')
<script src="{{ URL::asset('front-theme/js/rooms.js')}}"></script>
@endsection
