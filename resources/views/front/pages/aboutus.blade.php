@extends('front.page-template')

@section('title')
<title>About - Cocoon House</title>
@endsection

@section('meta')
<meta name="description" content="Bringing together International luxury and the warmest Kiwi hospitality Cocoon House is home to New Zealand’s most exquisite private events. Choose any combination of spaces or take over the whole house. With three floors of versatile and stunningly decorated spaces Cocoon offers both flexibility and exclusivity for receptions, meetings and special celebrations.">
<link rel="canonical" href="{{route('aboutus')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="About - Cocoon House">
<meta property="og:description" content="Bringing together International luxury and the warmest Kiwi hospitality Cocoon House is home to New Zealand’s most exquisite private events. Choose any combination of spaces or take over the whole house. With three floors of versatile and stunningly decorated spaces Cocoon offers both flexibility and exclusivity for receptions, meetings and special celebrations.">
<meta property="og:image" content="{{ URL::asset('front-theme/images/about_intro.jpg')}}">
<meta property="og:url" content="{{route('aboutus')}}">
<meta property="og:site_name" content="Cocoon House">

<meta name="twitter:title" content="About - Cocoon House">
<meta name="twitter:description" content="Bringing together International luxury and the warmest Kiwi hospitality Cocoon House is home to New Zealand’s most exquisite private events. Choose any combination of spaces or take over the whole house. With three floors of versatile and stunningly decorated spaces Cocoon offers both flexibility and exclusivity for receptions, meetings and special celebrations.">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/about_intro.jpg')}}">
<meta name="twitter:card" content="summary_large_image">
@endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about_responsive.css')}}">
@endsection

@section('breadcrumb')
<div class="home">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{ URL::asset('front-theme/images/about_header.jpg')}}" data-speed="0.8"></div>
		<div class="home_content">
			<div class="home_subtitle">Cocoon House</div>
			<div class="home_title">About</div>
		</div>
    </div>
</div>
@endsection

@section('content')
<div class="intro">
    <div class="container">
        <div class="row">

            <!-- Intro Content -->
            <div class="col-lg-7 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">
                        <div class="section_title_container">
                            <div class="section_subtitle">Cocoon House</div>
                            <div class="section_title"><h3>PRIVATE EVENTS</h3></div>
                        </div>
                        <div class="intro_text">
                            <p>Bringing together International luxury and the warmest Kiwi hospitality Cocoon House is home to New Zealand’s most exquisite private events.</p>
                            <p>Choose any combination of spaces or take over the whole house. With three floors of versatile and stunningly decorated spaces Cocoon offers both flexibility and exclusivity for receptions, meetings and special celebrations.</p>
                            <p>With truly world class service standards our staff are dedicated to making every Cocoon experience a luxurious delight for every guest. We can handle every detail for you, or simply support you creating your own unique vision.</p>
                            <p>Cocoon House is a truly boutique venue designed for exclusive and intimate gatherings.</p>
                        </div>
                        {{-- <div class="intro_link"><a href="rooms.html">View Rooms</a></div>
                        <a href="#" class="button_container intro_button"><div class="button text-center"><span>Book Your Stay</span></div></a> --}}
                    </div>
                </div>
            </div>

            <!-- Intro Image -->
            <div class="col-lg-5 intro_col">
                <div class="intro_image magic_up">
                    <!-- Image credit: https://unsplash.com/@ibrahimboran -->
                    <img style="margin-top:100px;" src="{{ URL::asset('front-theme/images/img_3.jpg')}}" alt="">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Milestones -->
{{-- <div style="height:3px;border-bottom:1px solid #c8c8c8; background:#e8e8e8;"></div> --}}
{{-- <div class="milestones">
    <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{ URL::asset('front-theme/images/milestones.jpg')}}" data-speed="0.8"></div>
    <div class="milestones_background_overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="milestones_title"><h2>Facts About Us</h2></div>
                <div class="milestones_text">
                    <p>Praesent fermentum ligula in dui imperdiet, vel tempus nulla ultricies. Phasellus at commodo ligula. Nullam molestie volutpat sapien, a dignissim tortor laoreet quis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
                </div>
            </div>
        </div>
        <div class="row milestones_row">


            <div class="col-lg-3 col-md-6 milestone_col">
                <div class="milestone d-flex flex-row align-items-center justify-content-md-center justify-content-start">
                    <div class="milestone_content">
                        <div class="milestone_counter" data-end-value="7">0</div>
                        <div class="milestone_text">Cocoon Spaces</div>
                    </div>
                </div>
            </div>


            <div class="col-lg-3 col-md-6 milestone_col">
                <div class="milestone d-flex flex-row align-items-center justify-content-md-center justify-content-start">
                    <div class="milestone_content">
                        <div class="milestone_counter" data-end-value="5">0</div>
                        <div class="milestone_text">Days</div>
                    </div>
                </div>
            </div>


            <div class="col-lg-3 col-md-6 milestone_col">
                <div class="milestone d-flex flex-row align-items-center justify-content-md-center justify-content-start">
                    <div class="milestone_content">
                        <div class="milestone_counter" data-end-value="57">0</div>
                        <div class="milestone_text">Guests</div>
                    </div>
                </div>
            </div>


            <div class="col-lg-3 col-md-6 milestone_col">
                <div class="milestone d-flex flex-row align-items-center justify-content-md-center justify-content-start">
                    <div class="milestone_content">
                        <div class="milestone_counter" data-end-value="30">0</div>
                        <div class="milestone_text">Bottles</div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div> --}}

<!-- Presentation Video -->

<div class="presentation_video" style="padding-bottom:100px;">
    <div class="container">
        <div class="row">

            <!-- Video -->
            <div class="col-lg-7 video_col">
                    <video width="100%" height="100%" controls autoplay>
                            <source src="{{ URL::asset('front-theme/video/cocoonstillfilm.mp4')}}" type="video/mp4">
                            {{-- <source src="movie.ogg" type="video/ogg"> --}}
                            Your browser does not support the video tag.
                        </video>
            </div>

            <!-- Content -->
            <div class="col-lg-5 video_col">
                <div class="video_content">
                    <div class="section_title_container">
                        <div class="section_subtitle">cocoon house</div>
                        <div class="section_title"><h2>WELCOME</h2></div>
                    </div>
                    <div class="video_text">

                        <p>A glimpse of the hidden spaces within. Every space curated for your comfort and luxury to host boutique experiences.</p>
                    </div>
                    {{-- <div class="testimonial">
                        <div class="testimonial_stars">
                            <ul class="d-flex flex-row align-items-start justfy-content-start">
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                        <div class="testimonial_text">“ Praesent fermentum ligula in dui imperdiet, vel tempus nulla ultricies. Phasellus at commodo ligula. Nullam molestie.</div>
                        <div class="testimonial_author d-flex flex-row align-items-center justify-content-start">
                            <div class="testimonial_author_image"><img src="images/testimonial.png" alt=""></div>
                            <div class="testimonial_author_name"><a href="#">Michael Smith</a><span>, Client</span></div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="{{ URL::asset('front-theme/js/about.js')}}"></script>
@endsection
