@extends('front.page-template')

@section('title')
<title>Contact - Cocoon House</title>
@endsection

@section('meta')
<meta name="description" content="We are based in Freemans Bay, on the periphery of the CBD, a short walk to Victoria Park and Wynyard Quarter. There is no designated parking at the venue.">
<link rel="canonical" href="{{route('contact')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="Contact - Cocoon House">
<meta property="og:description" content="We are based in Freemans Bay, on the periphery of the CBD, a short walk to Victoria Park and Wynyard Quarter. There is no designated parking at the venue.">
<meta property="og:image" content="{{ URL::asset('front-theme/images/about.jpg')}}">
<meta property="og:url" content="{{route('contact')}}">
<meta property="og:site_name" content="Cocoon House">

<meta name="twitter:title" content="Contact - Cocoon House">
<meta name="twitter:description" content="We are based in Freemans Bay, on the periphery of the CBD, a short walk to Victoria Park and Wynyard Quarter. There is no designated parking at the venue.">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/about.jpg')}}">
<meta name="twitter:card" content="summary_large_image">
@endsection

@section('style')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.css">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/contact.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/contact_responsive.css')}}">
@endsection

@section('breadcrumb')
<div class="home">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{ URL::asset('front-theme/images/home_slider_2.jpg')}}" data-speed="0.8"></div>
		<div class="home_content">
			<div class="home_subtitle">Cocoon House</div>
			<div class="home_title">Contact</div>
		</div>
    </div>
</div>
@endsection

@section('content')
<div class="intro">
    <div class="container">
        <div class="row">

            <!-- Contact -->

            <div class="contact">
                <div class="container">
                    <div class="row">

                        <!-- Contact Content -->
                        <div class="col-lg-4">
                            <div class="contact_content">
                                <div class="section_title_container">
                                    <div class="section_subtitle">cocoon</div>
                                    <div class="section_title"><h3>Say Hello</h3></div>
                                </div>
                                <div class="contact_text">
                                    <p>We’d love to hear from you to discuss any venue requirements, or help you to plan a special upcoming event with our events team. Connect here and we’ll be in touch asap.</p>
                                </div>
                                <div class="contact_info">
                                    <ul>
                                        <li>35 Spring St, Freemans Bay, Auckland 1011, New Zealand</li>
                                        <li>+64 274 904 152</li>
                                        <li>shelley.arrell@cocoonhouse.co.nz</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <!-- Contact Form -->
                        <div class="col-lg-8">
                            <div class="contact_form_container">
                                @if($message = Session::get('success'))
                                    <div class="col-md-12 text-center wow animated fadeInUp">
                                        <div class="alert alert-success alert-block">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                    </div>
                                @endif
                                <form action="{{route('send-mail')}}" method="POST" class="contact_form clearfix">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <input type="text" name="name" class="contact_input" placeholder="Your Name" required="required">
                                        </div>
                                        <div class="col-lg-6">
                                            <input type="email" name="email" class="contact_input" placeholder="Your E-mail" required="required">
                                        </div>
                                        <div class="col-lg-6">
                                            <input type="text" name="phone" class="contact_input" placeholder="Your Phone" required="required">
                                        </div>
                                        <div class="col-lg-6">
                                            <input type="number" name="attendees" class="contact_input" placeholder="Approx no. of attendees" required="required">
                                        </div>
                                        <div class="col-lg-12">
                                                <div class="input-group date">
                                                        <input type="text" name="date" placeholder="Event Date" class="contact_input" id="datepicker">
                                                        <div class="input-group-addon">
                                                            <span class="glyphicon glyphicon-th"></span>
                                                        </div>
                                                    </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <input type="text" name="subject" class="contact_input" placeholder="Subject">
                                        </div>
                                        <div class="col-lg-12">
                                                <textarea class="contact_input contact_textarea" name="message" placeholder="Message" required="required"></textarea>
                                        </div>
                                        <div class="col-lg-12">

                                            <div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
                                            @if ($errors->has('g-recaptcha-response'))
                                                <span class="invalid-feedback" style="display: block;">
                                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                                </span>
                                            @endif

                                        </div>
                                        <div class="col-lg-12">

                                            <input style="color:#fff;" type="submit"  class="contact_button" value="Send" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="{{ URL::asset('front-theme/js/contact.js')}}"></script>

@endsection
