@extends('front.page-template')

@section('title')
<title>Cocoon Partners - Cocoon House</title>
@endsection

@section('meta')
<meta name="description" content="Our preferred caterers will quote you the full event experience including
food, bar service, staff, crockery, cutlery and glassware as required.">
<link rel="canonical" href="{{route('story')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="Cocoon Partners - Cocoon House">
<meta property="og:description" content="Our preferred caterers will quote you the full event experience including
food, bar service, staff, crockery, cutlery and glassware as required.">
<meta property="og:image" content="{{ URL::asset('front-theme/images/rosa.jpg')}}">
<meta property="og:url" content="{{route('story')}}">
<meta property="og:site_name" content="Cocoon House">

<meta name="twitter:title" content="Cocoon Partners - Cocoon House">
<meta name="twitter:description" content="Our preferred caterers will quote you the full event experience including
food, bar service, staff, crockery, cutlery and glassware as required.">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/rosa.jpg')}}">
<meta name="twitter:card" content="summary_large_image">
@endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about_responsive.css')}}">
@endsection

@section('breadcrumb')
<div class="home">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{ URL::asset('front-theme/images/home_slider_2.jpg')}}" data-speed="0.8"></div>
		<div class="home_content">
			<div class="home_subtitle">Cocoon House</div>
			<div class="home_title">Our Partners</div>
		</div>
    </div>
</div>
@endsection

@section('content')
<div class="intro">
    <div class="container">
        <div class="row">
            @if(Carbon\Carbon::today()->format('d/m/Y') <= '30/06/2020')
            <div class="col-lg-12" style="text-align:center;margin:auto;">
                <div class="row">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4">
                        <div class="intro_container1 magic_up" style="text-align:center;margin:auto;">
                        <a href="https://www.breastcancerfoundation.org.nz/" target="_blank"><img src="{{ URL::asset('front-theme/images/partners/BCFNZ.png')}}" width="90%" style="padding:15px;margin-top:0px;" alt="Great Catering"></a>

                        </div>
                    </div>
                    <div class="col-lg-4"></div>
                </div>
            </div>
            @endif
            <div class="col-lg-12" style="text-align:center;margin:auto;">
                <div class="row">
                    <div class="col-lg-4"> <a href="https://greatcatering.co.nz/" target="_blank"><img src="{{ URL::asset('front-theme/images/partners/great-catering.png')}}" width="80%" style="padding:15px;margin-top:0px;" alt="Great Catering"></a></div>
                    <div class="col-lg-4">
                            <a href="https://www.urbangourmet.co.nz/" target="_blank"><img src="{{ URL::asset('front-theme/images/partners/urban-gourmet.png')}}" width="80%" style="padding:15px;margin-top:0px;" alt="Urban Gourmet"></a>
                    </div>
                    <div class="col-lg-4"><a href="https://www.blackpineapple.co.nz" target="_blank"><img src="{{ URL::asset('front-theme/images/partners/BPC_primary_blk.png')}}" width="80%" style="padding:15px;margin-top:0px;" alt="Urban Gourmet"></a></div>
                </div>
            </div>


            <div class="col-lg-12" style="text-align:center;margin:auto;">

                <p style="margin-top:10px;">
                    Our preferred caterers will quote you the full event experience including
                    food, bar service, staff, crockery, cutlery and glassware as required.
                </p>

            </div>
        </div>
    </div>
</div>


@endsection

@section('script')
<script src="{{ URL::asset('front-theme/js/about.js')}}"></script>
@endsection
