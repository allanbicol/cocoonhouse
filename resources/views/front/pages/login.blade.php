@extends('front.login-template')

@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about_responsive.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/login/vendor/animate/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/login/vendor/css-hamburgers/hamburgers.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/login/css/util.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/login/css/main.css')}}">
@endsection

@section('breadcrumb')
{{-- <div class="home">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{ URL::asset('front-theme/images/home_slider_2.jpg')}}" data-speed="0.8"></div>
		<div class="home_content">
			<div class="home_subtitle">Cocoon House</div>
			<div class="home_title">Room Details</div>
		</div>
    </div>
</div> --}}
@endsection

@section('content')
<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="{{ URL::asset('front-theme/images/Cocoon-House-Gold.png')}}" alt="IMG">
				</div>

				<form class="login100-form validate-form">
					<span class="login100-form-title">
						Member Login
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" type="text" name="email" placeholder="Email" autocomplete="off">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="password" name="pass" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
					</div>

					<div class="text-center p-t-12">
						<span class="txt1">
							Forgot
						</span>
						<a class="txt2" href="#">
							Username / Password?
						</a>
					</div>

					{{-- <div class="text-center p-t-136">
						<a class="txt2" href="#">
							Create your Account
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
						</a>
					</div> --}}
				</form>
			</div>
		</div>
	</div>

@endsection
@section('script')
<script src="{{ URL::asset('front-theme/js/about.js')}}"></script>
{{-- <script src="{{ URL::asset('front-theme/login/vendor/bootstrap/js/popper.js')}}"></script> --}}
<script src="{{ URL::asset('front-theme/login/vendor/select2/select2.min.js')}}"></script>
<script src="{{ URL::asset('front-theme/login/vendor/tilt/tilt.jquery.min.js')}}"></script>
<script >
    $('.js-tilt').tilt({
        scale: 1.1
    })
</script>
<script src="{{ URL::asset('front-theme/login/js/main.js')}}"></script>
@endsection
