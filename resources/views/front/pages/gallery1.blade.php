@extends('front.page-template')

@section('title')
<title>ART + WINE + DESIGN - Cocoon House</title>
@endsection

@section('meta')
<meta name="description" content="Our preferred caterers will quote you the full event experience including
food, bar service, staff, crockery, cutlery and glassware as required.">
<link rel="canonical" href="{{route('story')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="Cocoon Partners - Cocoon House">
<meta property="og:description" content="Our preferred caterers will quote you the full event experience including
food, bar service, staff, crockery, cutlery and glassware as required.">
<meta property="og:image" content="{{ URL::asset('front-theme/images/rosa.jpg')}}">
<meta property="og:url" content="{{route('story')}}">
<meta property="og:site_name" content="Cocoon House">

<meta name="twitter:title" content="Cocoon Partners - Cocoon House">
<meta name="twitter:description" content="Our preferred caterers will quote you the full event experience including
food, bar service, staff, crockery, cutlery and glassware as required.">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/rosa.jpg')}}">
<meta name="twitter:card" content="summary_large_image">
@endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about_responsive.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/isotope.css')}}">
@endsection

@section('breadcrumb')
<div class="home">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{ URL::asset('front-theme/images/home_slider_8.jpg')}}" data-speed="0.8"></div>
		<div class="home_content">
			<div class="home_subtitle">Cocoon House</div>
			<div class="home_title">ART + WINE + DESIGN</div>
		</div>
    </div>
</div>
@endsection

@section('content')
<div class="intro">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 intro_col" style="margin-bottom:60px;">
                <div class="intro_content">
                    <div class="section_title_container">
                        <div class="section_subtitle">Cocoon House</div>
                        <div class="section_title"><h3>Collections</h3></div>
                    </div>
                </div>
            </div>

            <!-- Intro Image -->
            <div class="col-lg-5 intro_col">
                <div class="intro_image magic_up">
                    <div>
                        <img style="margin-top:100px;" src="{{ URL::asset('front-theme/images/collections/out_of_nothing.jpg')}}" alt="">
                        <span style="line-height: -10px;font-size:9px;display:inline-block;padding-top:10px;">Out Of Nothing, 2018 <br>acrylic and oil glaze on linen  <br>1200 x 950mm</span>
                    </div>
                </div>
            </div>
            <!-- Intro Content -->
            <div class="col-lg-7 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">
                        <div class="section_title_container">
                            <div class="section_title" style="text-align: right;"><h3>Kathy <br><span style="color:#a8894c">Barber</span></h3></div>
                        </div>
                        <div class="intro_text" style="text-align:justify;">
                            <p style="font-size:12px;">Born in Palmerston North in 1965, Kathy Barber now lives and works in Auckland. Barber has regularly exhibited in New Zealand and Australia since 2000, and has work in private collections throughout New Zealand, Sydney, Melbourne and Germany. She is represented in Auckland by Orexart Gallery and with Chambers Gallery in Christchurch</p>
                            <p style="font-size:12px;">Kathy Barber's paintings develop out of landscape, language, thoughts and emotions. Barber entwines layers of gestural marks and abstracted writings to create representations of thoughts, forming entangled structures that are curling and complex, delicate and dense. Thoughts, these works suggest, are like intricate knots, their logic fine but elusive.</p>
                            <p style="font-size:12px;">Barber’s paintings are in a sense a palimpsest. If as in textural studies, a palimpsest is a manuscript or a very old document from which the writing has been removed and the surface rewritten. So it is that Barber’s paintings can be read as if there are many levels of meaning. In essence they are paintings where the subtext remains but the thoughts have been rubbed away. What remains is the encounter.</p>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="row" style="background-color:#fff;">
            <div class="col-lg-7 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">

                        <div class="intro_text" style="text-align:justify;">
                            <p style="font-size:12px;"><b>Kathy Barber </b><br>Blessings, 2017 <br>acrylic and oil glaze on linen <br>800mm diameter</p>
                        </div>
                        {{-- <div class="intro_link"><a href="rooms.html">View Rooms</a></div>
                        <a href="#" class="button_container intro_button"><div class="button text-center"><span>Book Your Stay</span></div></a> --}}
                    </div>
                </div>
            </div>
            <!-- Intro Image -->
            <div class="col-lg-5 intro_col">
                    <div class="intro_image magic_up">
                        <div>
                            <img src="{{ URL::asset('front-theme/images/collections/blessings.jpg')}}" alt="">
                        </div>
                    </div>
                </div>

        </div>

        <div class="row">
            <!-- Intro Image -->
            <div class="col-lg-5 intro_col">
                <div class="intro_image magic_up">
                    <div>
                        <img style="margin-top:100px;" src="{{ URL::asset('front-theme/images/collections/paul_dibble.jpg')}}" alt="">
                        <span style="line-height: -10px;font-size:9px;display:inline-block;padding-top:10px;">Out Of Nothing, 2018 <br>acrylic and oil glaze on linen  <br>1200 x 950mm</span>
                    </div>
                </div>
            </div>
            <!-- Intro Content -->
            <div class="col-lg-7 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">
                        <div class="section_title_container">
                            <div class="section_title" style="text-align: right;"><h3>Paul <br><span style="color:#a8894c">Dibble</span></h3></div>
                        </div>
                        <div class="intro_text" style="text-align:justify;">
                            <p style="font-size:12px;">Paul Dibble is one of New Zealand’s foremost senior sculptors. He is renowned for works that engage a range styles from surrealism and folk art, to a cool elegant modernism. The human figure, New Zealand and Pacific narratives, and objects from contemporary life form the subjects of his work which vary in size from small maquettes to large works over five metres in height. Ideas emerge as beautiful fluid line drawings which are worked and reworked to a point of perfect balance before being modelled and cast through a process of ceramic shell, lost wax or sand casting. The final sculptures are finished with a range of patinas - from a rich pastoral green, to a golden brown and a deep, earthy black. Dibble's sculpture has been included in numerous exhibitions since the early 1970s and has been consistently coveted for major public and private commissions. The undeniable highlight of his career to date was securing the commission of The New Zealand Hyde Park Corner Memorial in London in 2006. </p>
                            <p style="font-size:12px;">Gow Langsford Gallery has represented Paul Dibble since 1990.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="background-color:#fff;">
            <div class="col-lg-7 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">

                        <div class="intro_text" style="text-align:justify;">
                            <p style="font-size:12px;"><b>Paul Dibble </b><br>Sneaking Along, 2016 <br>edition 2 of 5  cast bronze  <br>360 x 310 x 310mm</p>
                        </div>
                        {{-- <div class="intro_link"><a href="rooms.html">View Rooms</a></div>
                        <a href="#" class="button_container intro_button"><div class="button text-center"><span>Book Your Stay</span></div></a> --}}
                    </div>
                </div>
            </div>
            <!-- Intro Image -->
            <div class="col-lg-5 intro_col">
                    <div class="intro_image magic_up">
                        <div style="text-align:right">
                            <img src="{{ URL::asset('front-theme/images/collections/sneaking_along.jpg')}}" width="80%" alt="">
                        </div>
                    </div>
                </div>

        </div>

        <div class="row">
            <!-- Intro Image -->
            <div class="col-lg-5 intro_col">
                <div class="intro_image magic_up">
                    <div>
                        <img style="margin-top:100px;" src="{{ URL::asset('front-theme/images/collections/hope.jpg')}}" alt="">
                        <span style="line-height: -10px;font-size:9px;display:inline-block;padding-top:10px;">Hope, 2018  <br>acrylic on canvas <br>1000 x 1000mm</span>
                    </div>
                </div>
            </div>
            <!-- Intro Content -->
            <div class="col-lg-7 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">
                        <div class="section_title_container">
                            <div class="section_title" style="text-align: right;"><h3>Luo <br><span style="color:#a8894c">Fang</span></h3></div>
                        </div>
                        <div class="intro_text" style="text-align:justify;">
                            <p style="font-size:12px;">Luo Fang is from Si Chuan China. He is a quiet person and loves thinking. Luo enjoys being alone and always finds happiness from it.  A dried fish, a walking stick and a rusty key were hanging on the mottled wall.  To Fang, the juxtaposition tells a story.  No matter how hard life is, never give up, fill your heart with hope.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <!-- Intro Content -->
            <div class="col-lg-7 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">
                        <div class="section_title_container">
                            <div class="section_title" style="text-align: left;"><h3>Otis <br><span style="color:#a8894c">Frizzell</span></h3></div>
                        </div>
                        <div class="intro_text" style="text-align:justify;">
                            <p style="font-size:12px;">“Artist, hip hop performer, radio host, tattooist, graphic designer.”</p>
                            <p style="font-size:12px;">Otis Frizzell has maintained a high profile for more than a decade, bringing the same appealing combination of energy, humour and raw talent to all his work, regardless of the medium. </p>
                            <p style="font-size:12px;">Otis has more than fifteen years of public graffiti art experience and since 1998 has retained his undisputed position as one of New Zealand’s highest profile graffiti artists. </p>
                            <p style="font-size:12px;">His first solo exhibition ‘Opto2000’ was produced with pop culture manipulator Mike Weston. This exhibition marked the beginning of an innovative art production and management collaboration. The two of them have produced a playful and challenging stream of works, setting and resetting the benchmark in artistic and technical achievement against which the local scene has measured itself. Otis’ works can be found in the Saatchi & Saatchi offices, IE Music (Robbie William’s London Management Office), on KFC packaging, Playstation advertisements, TV2 promos and record sleeves. His December 2004 cover illustration for ProDesign won the ‘Business Magazine Cover of the Year Award’. During 2004 Otis abandoned TV celebrity to focus on art projects working out of ‘The Area’ studio with Mike Weston. In December 2004 this duo collaborated with well known Maori activist Tame Iti, producing ‘Dr Tutu featuring Tame Iti’: creating an art/music package exploring themes of appropriation, biculturalism and theft, presented as an exhibition of 88 paintings inspired by the recorded words featured on a CD accompanying the works. Artistic innovator, style leader, radio personality, tattooist, graphic designer, hip hop performer – a man of many talents and a success in every pursuit, Otis is one of the true stars of New Zealand’s alternative universe. As well as making art, he also own and runs The Lucky Taco food truck/retail business with his wife Sarah.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Intro Image -->
            <div class="col-lg-5 intro_col">
                <div class="intro_image magic_up">
                    <div>
                        <img style="margin-top:100px;" src="{{ URL::asset('front-theme/images/collections/otis_fritzzell.jpg')}}" alt="">
                        {{-- <span style="line-height: -10px;font-size:9px;display:inline-block;padding-top:10px;">Hope, 2018  <br>acrylic on canvas <br>1000 x 1000mm</span> --}}
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="background-color:#fff;">
            <div class="col-lg-7 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">

                        <div class="intro_text" style="text-align:justify;">
                            <p style="font-size:12px;"><b>Otis Frizzell</b><br>Brick Balloons, 2018  <br>hand drawn with hand cut stencils  <br>and enamel spray paint</p>
                        </div>
                        {{-- <div class="intro_link"><a href="rooms.html">View Rooms</a></div>
                        <a href="#" class="button_container intro_button"><div class="button text-center"><span>Book Your Stay</span></div></a> --}}
                    </div>
                </div>
            </div>
            <!-- Intro Image -->
            <div class="col-lg-5 intro_col">
                    <div class="intro_image magic_up">
                        <div style="text-align:right">
                            <img src="{{ URL::asset('front-theme/images/collections/brick_ballons.jpg')}}" style="padding:10px;" alt="">
                        </div>
                    </div>
                </div>

        </div>

        <div class="row">
            <!-- Intro Image -->
            <div class="col-lg-4 intro_col">
                <div class="intro_image magic_up">
                    <div>
                        <img style="margin-top:100px;" src="{{ URL::asset('front-theme/images/collections/hipnos.jpg')}}" alt="">
                        <span style="line-height: -10px;font-size:9px;display:inline-block;padding-top:10px;">Hypnos, 2017   <br>charcoal on paper  <br>1270 x 750mm</span>
                    </div>
                </div>
            </div>
            <!-- Intro Content -->
            <div class="col-lg-8 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section_title_container">
                                    <div class="section_title" style="text-align: right;"><h3>Liam <br><span style="color:#a8894c">Gerrard</span></h3></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <p style="font-size:9px;">Luo Fang is from Si Chuan China. He is a quiet person and loves thinking. Luo enjoys being alone and always finds happiness from it.  A dried fish, a walking stick and a rusty key were hanging on the mottled wall.  To Fang, the juxtaposition tells a story.  No matter how hard life is, never give up, fill your heart with hope.</p>
                                <p style="font-size:9px;">Over the years Gerrard has created finely detailed charcoal and graphite drawings that ride on the ever-mounting raft of source material available to contemporary image makers. Animals, arcane objects, cultural icons, entertainers, historical figures, political leaders, sporting stars, rogues and rebels. But what lurks beneath the surface of these often dark, humourous and arresting renderings? Gerrard draws our attention to the complexities of the image amalgam and the resonant consequences when associated meanings are pile-driven with purpose to the surface of the paper.</p>
                                <p style="font-size:9px;">Soil and Salt (2017), his most recent exhibition, is comprised of works that cast stoic maidens of yore amid atmospheric and preternatural settings. Who are these women? Where and when are they from? At first glance the works might evoke a sense of nostalgia for an art of the past, inherited photographs, someone once known. Look a little closer: this is no mere wistful jaunt down memory lane. The impression that these works are a revision of Victorian or Edwardian-era portraits soon fades, sharply contrasted in the gnarling, obscured and symbolic nature of the misty settings each haunted phantasm emerges from. </p>
                                <p style="font-size:9px;">There is something at once alluring and unsettling about these images that somehow attracts and repulses via equivocal means. In Maw (2017) and Pale on Pale (2017) these hidden apparitions coalesce as something more primal and mysterious, forever trapped in a flux of bloom and decay, </p>
                            </div>
                            <div class="col-lg-6">
                                <p style="font-size:9px;">unfurling out of the dim light between night and day. Hypnos (2017) and Go on, Stay Low (2017) sit avoidant and defiant, each possessed by an earthy essence and ancient power, beckoning the viewer closer in seductive and sinister ways.</p>
                                <p style="font-size:9px;">The working title for Soil and Salt was ‘Carrion Flowers,’ referring to a genera of plants, also known as Corpse Flowers, which emit an odour like rotting flesh. While most varieties attract flies and beetles as pollinators, there are some that trap insects to make certain that pollen is gathered and transferred. As direct as this reference may seem, Gerrard arrived at the title via the aesthetic and sonic sensibilities of the track by same name by American musician Chelsea Wolfe. As a musician himself and fan of the heavier genres of rock music, Gerrard subtly draws from the well of associated idiosyncratic album covers, tee shirt graphics and music videos. What flourishes when these influences are married with art historical tropes is a type of Promethean evolution. </p>
                                <p style="font-size:9px;">If you were to ask where he gets his ideas from Gerrard might quip “an old shop in Pokeno, they do a roaring trade in pigs heads out the front and little-known ideas out the back.” This of course would be a puckish reply for what are undoubtedly innate abilities and involved processes, the likes of which trawl and sprawl the vastness of historic and contemporary paradigms. From artists’ studios, to band rehearsal spaces, to gymnasium locker rooms, to student radio booths, to TAB pubs, to media portrayals, to the internet – grimly beautiful and tragically funny things can be found in dark corners. By combining an acute awareness to the possibilities of the image, other art forms, his interests and experiences, Gerrard flips the familiar and obscure with an enviable assuredness which plays out like a game of hustled heads and tails. </p>
                                <p style="font-size:9px;">A penny for your thoughts? Or perhaps one for the ferryman on the silent ride over. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <!-- Intro Image -->
            <div class="col-lg-4 intro_col">
                <div class="intro_image magic_up">
                    <div>
                        <img style="margin-top:100px;" src="{{ URL::asset('front-theme/images/collections/max_gimblett.jpg')}}" alt="">
                        {{-- <span style="line-height: -10px;font-size:9px;display:inline-block;padding-top:10px;">Hypnos, 2017   <br>charcoal on paper  <br>1270 x 750mm</span> --}}
                    </div>
                </div>
            </div>
            <!-- Intro Content -->
            <div class="col-lg-8 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section_title_container">
                                    <div class="section_title" style="text-align: right;"><h3>Max <br><span style="color:#a8894c">Gimblet</span></h3></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <p style="font-size:9px;">Max Gimblett is a prominent New Zealand painter.  His philosophies and practices encompass influences as varied as Abstract Expressionism, Modernism, Eastern and Western spiritual beliefs, Jungian psychology and ancient cultures.</p>
                                <p style="font-size:9px;">In 2009 Gimblett became one of the first New Zealand artists to have an artwork exhibited in the Guggenheim in New York. Lion (1985) was included in an exhibition titled The Third Mind: American Artists Contemplate Asia, 1860-1989. The piece hung next to works by international heavyweight artists such as Robert Motherwell, James Whistler, Robert Rauschenberg, Nam June Paik, John Cage and Yoko Ono and is now part of the Museum's permanent collection. </p>
                                <p style="font-size:9px;">His international significance was further cemented with his inclusion in another major exhibition in the United States, this time at the Warhol Museum in Pittsburgh, Pennsylvania in 2011. The exhibition The Word of God, was a series of one-person shows representing each of the world’s five major religions; Islam, Judaism, Christianity, Buddhism and Hinduism. Gimblett was the representative of Buddhism.</p>
                            </div>
                            <div class="col-lg-6">
                                <p style="font-size:9px;">Born in New Zealand, Gimblett has been primarily based in New York since 1972, and continues to exhibit regularly in both locations.  This mix of cultures and aesthetics is evident in Gimblett’s work, which consists largely of object based paintings.  His shaped canvases convey various associations and meanings connected to the oval, rectangle, tondo, keystone, and the quatrefoil, for which Gimblett is most recognised.  The use of the quatrefoil refers to a multiplicity of meanings as it dates back to preChristian times and is found in both Western and Eastern religions symbolising such objects as a rose, window, cross and lotus.  </p>
                                <p style="font-size:9px;">Combinations of gold, silver, copper, bronze, epoxy, resin, plaster, paint and pigments create extremely delicate surfaces which are often interrupted by bold gestural brush marks in acrylic polymers and paints. The religious associations of his materials, particularly the association of precious metals, to honour, wisdom, enlightenment and energies also reiterates his exploration of spiritual beliefs. Alongside his paintings Gimblett produces an ongoing series of works on paper.  In 2002 a survey of these works were exhibited at the Queensland Art Gallery in Australia.  </p>
                                <p style="font-size:9px;">A major monograph on Gimblett’s work was published in 2002 to accompany his survey exhibition at Auckland Art Gallery. The book maps the development of his exceptional career, from the rarely seen geometric paintings of the 1970’s, through a myriad shapes and techniques, to his works from the early 2000s that are just as stylistically diverse.   </p>
                                <p style="font-size:9px;">Max Gimblett is represented by Gow Langsford Gallery.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row" style="background-color:#fff;">
            <div class="col-lg-7 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">

                        <div class="intro_text" style="text-align:justify;">
                            <p style="font-size:12px;"><b>Max Gimblett</b><br>Royal Purple Fleece, 2016<br>Gesso, acrylic & vinyl polymers,  <br>resin, sealed Japanese Blonde Silver <br>colored silver leaf on canvas <br>50 x 50 x 2 inches</p>
                        </div>
                        {{-- <div class="intro_link"><a href="rooms.html">View Rooms</a></div>
                        <a href="#" class="button_container intro_button"><div class="button text-center"><span>Book Your Stay</span></div></a> --}}
                    </div>
                </div>
            </div>
            <!-- Intro Image -->
            <div class="col-lg-5 intro_col">
                    <div class="intro_image magic_up">
                        <div style="text-align:right">
                            <img src="{{ URL::asset('front-theme/images/collections/royal_purple.png')}}" alt="">
                        </div>
                    </div>
                </div>

        </div>

        <div class="row">
            <!-- Intro Image -->
            <div class="col-lg-7 intro_col">
                <div class="intro_image magic_up">
                    <div>
                        <img style="margin-top:100px;" src="{{ URL::asset('front-theme/images/collections/twin_swallows.jpg')}}" alt="">
                        <span style="line-height: -10px;font-size:9px;display:inline-block;padding-top:10px;">Twin Swallows   <br>Authorized screenprint  <br>Based on the painting Twin  <br>Swallows, 1981</span>
                    </div>
                </div>
            </div>
            <!-- Intro Content -->
            <div class="col-lg-5 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section_title_container">
                                    <div class="section_title" style="text-align: right;"><h3>Wu <br><span style="color:#a8894c">Guanzhong</span></h3></div>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <p style="font-size:9px;">Wu Guanzhong ( 吴冠中 29 August 1919 – 25 June 2010)  was a contemporary Chinese painter widely recognized as a founder of modern Chinese painting. He is considered to be one of the greatest contemporary Chinese painters. Wu's artworks had both Western and Eastern influences, such as the Western style of Fauvism and the Eastern style of Chinese calligraphy. Wu had painted various aspects of China, including much of its architecture, plants, animals, people, as well as many of its landscapes and waterscapes in a style reminiscent of the impressionist painters of the early 1900s. He was also a writer on contemporary Chinese art. </p>
                                <p style="font-size:9px;">Among many of Wu Guanzhong’s paintings, Twin Swallows was the most outstanding and representative of his search for a synthesis of Western elements into traditional Chinese painting. It is a painting that captures both the static form of traditional Jiangnan architecture and the motion of two swallows as they fly toward a tree. Geometric shapes, especially rectangles, dominate half of the painting. For example, the front walls of the houses are horizontally placed white rectangles with simple black and gray lines to depict the edges and rooftop of each building. Doorways were painted in the same minimalistic manner, yet the contrast
                                    between the darkness inside the building with the lighter door frame is prominent enough to create a sense of depth. Wu’s attention to perspective and depth in Twin Swallows is a factor that distinguishes himself from many other traditional guohua painters. Although the white walls in Twin Swallows may seem like the dominating elements in this painting, it is in fact the pair of swallows that reveals Wu’s intention behind this painting. In the 1950s, Wu returned from France to his homeland. It was also a time when Chinese art entered the phase of socialist realism.  This artistic movement had encouraged many artists to create artwork in order to contribute
                                     to the Chinese society. Similarly, Wu felt obligated to pass on the knowledge that he had gained in France to the younger generations in China in order to promote the idea of a synthesis among Western and traditional Chinese art.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <!-- Intro Image -->
            <div class="col-lg-7 intro_col">
                <div class="intro_image magic_up">
                    <div>
                        <img style="margin-top:100px;" src="{{ URL::asset('front-theme/images/collections/ray_haydon.jpg')}}" alt="">
                        <span style="line-height: -10px;font-size:9px;display:inline-block;padding-top:10px;">Untitled, 2017  <br>carbon fibre, timber veneer  <br>2500 x 1100 x 350mm</span>
                    </div>
                </div>
            </div>
            <!-- Intro Content -->
            <div class="col-lg-5 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section_title_container">
                                    <div class="section_title" style="text-align: right;"><h3>Ray <br><span style="color:#a8894c">Haydon</span></h3></div>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <p style="font-size:11px;">Negative space is defined by the solid objects it surrounds. This is one of the physical aspects of Ray Haydon’s sculptural practice that really matters; he is actively “painting” the air with materials to fabricate constructions in space. </p>
                                <p style="font-size:11px;">In the same spirit of the Russian Constructivists like Naum Gabo, Haydon understands and is constantly testing the variable ways that materials such as steel, wood and carbon fibre can take on extraordinary new forms – pure materiality, monochromatic economy and complicated structures made to maximise spatial dynamics. </p>
                                <p style="font-size:11px;">His recent works from the Manoeuvre and Knot series are crafted from carbon fibre and timber veneer and give form to graceful movement such as one might expect to see in the ribbon event from an Olympic rhythmic gymnast. The works’ open-ended and interweaving lengths vary in “knottedness” underscoring the expert understanding of materiality that Haydon exploits.</p>
                                <p style="font-size:11px;">Haydon is constantly considering the unending physical propositions that lay in wait within the materials. With the most unforgiving and difficult media to work with, Haydon bends, twists and forms works that show a fluidity of movement from the most rigid beginnings.</p>
                                <p style="font-size:11px;"><b>Excerpts from an essay by Leafa Wilson
                                </b></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <!-- Intro Image -->
            <div class="col-lg-4 intro_col">
                <div class="intro_image magic_up">
                    <div>
                        <img style="margin-top:100px;" src="{{ URL::asset('front-theme/images/collections/gregor_kregar.jpg')}}" alt="">
                        {{-- <span style="line-height: -10px;font-size:9px;display:inline-block;padding-top:10px;">Hypnos, 2017   <br>charcoal on paper  <br>1270 x 750mm</span> --}}
                    </div>
                </div>
            </div>
            <!-- Intro Content -->
            <div class="col-lg-8 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section_title_container">
                                    <div class="section_title" style="text-align: right;"><h3>Gregor<br><span style="color:#a8894c">Kregar</span></h3></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <p style="font-size:9px;">Gregor Kregar is a consummate multi-media artist, never restricting himself to a single medium, and incorporating a vast range of materials. Best known for his often mammoth scale installations, his sculptural practice will regularly see stainless steel, plastic, ceramic, and glass alongside found objects such as recycled timber and inorganic rubbish. His playful sculptures toy with familiarity and the uncanny; Kregar moulds recognisable subjects and displaces them in new surroundings, confronting the viewer with unease, and calling into question an object’s socio-economic and political associations</p>
                                <p style="font-size:9px;">Previous bodies of work, including Matthew 12:12 Rugby World Cup series (2011), have explored the artist's role in the creation of a national cultural identity, while later, site-specific works such as Paradox Void (2017) in Christchurch, Perun Cloud (2017) in Melbourne, and Glass Room (2017), Sculpture on the Gulf, Waiheke Island, have encouraged audiences to reconsider their architectural surroundings, challenging preconceived notions of what constitutes ‘good’ and ‘successful’ building.</p>
                            </div>
                            <div class="col-lg-6">
                                <p style="font-size:9px;">Kregar’s site-specific sculptures are the subject of many New Zealand and international commissions. Most recently in August 2018, Kregar installed the playful and immersive Anthropocene Shelter as part of Curious Creatures & Marvellous Monsters at Te Papa Tongarewa. Other significant commissions have included Paradox Void 2 (2018) for the Symphony on the Seas cruise liner, Liquid Geometry 3 (2013), Lekhwiya Stadium, Doha, Qatar, Clouds And Cumulous Pavilion For Richard Pearse (2012), Christchurch International Airport, and Twisting The Void 2 (2010), Cornish, Abu Dhabi, United Arab Emirates. </p>
                                <p style="font-size:9px;">Kregar is the recipient of multiple awards and residencies, among them the Lexus Premier and People’s Choice Awards at Headland Sculpture on the Gulf (2013), ANZ Private Bank and Art & Australia Contemporary Art Award, Australia (2009) and the Paramount Winner, James Wallace Art Award (2000). International residencies include the McColl Art Centre, Charlotte, United States, and Monash University/Shepparton Art Gallery, South Project Residency, Melbourne, Australia, among others. </p>
                                <p style="font-size:9px;">His work is held in significant public collections including Te Papa Tongarewa, Auckland Art Gallery Toi o Tamaki, The Chartwell Collection, and the James Wallace Arts Trust. Kregar has a Bachelor of Fine Arts (Academy of Fine Arts, University of Ljubljana, 1996) and a Masters of Fine Arts (Elam School of Fine Arts, University of Auckland, 1999). </p>
                                <p style="font-size:9px;">Gregor Kregar is represented by Gow Langsford Gallery.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row" style="background-color:#fff;">

            <!-- Intro Image -->
            <div class="col-lg-12 intro_col">
                <div class="intro_image magic_up">
                    <div style="text-align:left">
                        <img style="padding-top:10px;" src="{{ URL::asset('front-theme/images/collections/i_disappear.jpg')}}" alt="">
                        <span style="line-height: -10px;font-size:9px;display:inline-block;padding-top:10px;"><b>Gregor Kregar</b><br>I Disappear (Green and Orange), 2013 ceramic <br>38 x 17 x 11mm - 178 x 74 x 45mm</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 intro_col">
                <div class="intro_image magic_up">
                    <div style="text-align:left">
                        <img style="padding-top:10px;" src="{{ URL::asset('front-theme/images/collections/glass_Gnome.jpg')}}" alt="">
                        <span style="line-height: -10px;font-size:9px;display:inline-block;padding-top:10px;"><b>Gregor Kregar</b><br>Glass Gnome (Light Blue), 2017     cast glass <br>220 x 100 x 80mm</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 intro_col">
                <div class="intro_image magic_up">
                    <div style="text-align:right">
                        <img style="padding-top:10px;" src="{{ URL::asset('front-theme/images/collections/hand_glazed.jpg')}}" alt="">
                        <span style="line-height: -10px;font-size:9px;display:inline-block;padding-top:10px;"><b>Gregor Kregar</b><br>I Disappear (Green and Orange), 2013 ceramic <br>38 x 17 x 11mm - 178 x 74 x 45mm</span>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <!-- Intro Image -->
            <div class="col-lg-4 intro_col">
                <div class="intro_image magic_up">
                    <div>
                        <img style="margin-top:115px;" src="{{ URL::asset('front-theme/images/collections/colin_mccahon.jpg')}}" alt="">
                        {{-- <span style="line-height: -10px;font-size:9px;display:inline-block;padding-top:10px;">Hypnos, 2017   <br>charcoal on paper  <br>1270 x 750mm</span> --}}
                    </div>
                </div>
            </div>
            <!-- Intro Content -->
            <div class="col-lg-8 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section_title_container">
                                    <div class="section_title" style="text-align: right;"><h3>Colin<br><span style="color:#a8894c">McCahon</span></h3></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <p style="font-size:9px;">Colin McCahon (1919-1987) is widely regarded as New Zealand’s foremost painter and is undoubtedly New Zealand’s most prominent twentieth century artist. As both an artist and writer, his work encompassed many themes, subjects and styles, as diverse as landscape through to figuration and abstraction. Through aspects of modernist painting, he traversed his relationship with the New Zealand landscape and with deep spiritual matters. McCahon was born in Timaru in 1919 and spent most of his youth in Dunedin. He attended Otago Boys’ High School and, while his experience there was largely unhappy, he found some enjoyment in the weekend art lessons he received from Russell Clark.</p>
                                <p style="font-size:9px;">In 1937 McCahon enrolled at the Dunedin School of Art. During his two years there he was introduced to many people who would influence his later work, including teachers Robert Field, Gordon Tovey and Douglas Charlton Edgar, and students Doris Lusk, his future wife Anne Hamblett (a former student and friend of influential New Zealand artist Toss Woollaston) and Rodney Kennedy.</p>
                                <p style="font-size:9px;">McCahon’s early landscape paintings reveal his interest in Cezanne, Gauguin and the cubists; influences that were revived in 1951 after he visited Melbourne cubist painter Mary Cockburn-Mercer. In 1946 the first of his religious paintings emerged, illustrating the beginning of McCahon’s lifelong exploration of themes relating to faith, spirituality, life and death. Many of these works, such as The King of the Jews of 1947, incorporated text within the painting, often in the form of cartoon-inspired speech bubbles which enabled the biblical figures to speak.</p>
                                <p style="font-size:9px;">This use of text later expanded to include writing in Maori, writing by New Zealand poet John Caselberg and the incorporation of numbers. Employed to aid him in better communicating with the viewers of his work, McCahon’s attraction to text was in part inspired by his recollections of the</p>
                            </div>
                            <div class="col-lg-6">
                                <p style="font-size:9px;">sign-writing on shop windows he had seen and admired as a child. </p>
                                <p style="font-size:9px;">A pivotal moment in McCahon’s career came in 1958 when a Carnegie Grant enabled him to paint and study in the USA.  After seeing in person work by many of Europe and America’s great painters, McCahon returned to New Zealand with a new appreciation for large scale work. He began to create ‘paintings to walk by’, resulting in large landscape abstractions on panels of unstretched canvas. </p>
                                <p style="font-size:9px;">After periods working at the Auckland public art gallery and as a lecturer at the Elam School of Fine Arts in Auckland, McCahon devoted himself to painting full time from 1970. </p>
                                <p style="font-size:9px;">McCahon’s relationship with the Auckland’s western suburbs played a significant role in much of his work.  After living in West Auckland’s Titirangi for a period during the 1950s, McCahon later established a studio at Muriwai Beach where several major series based in this part of Auckland were produced. Many of these works, dating from the 1970s, relate to Auckland’s west coast and were inspired by McCahon’s environmental concerns for the region, prompted initially by his observations of the gannet colony living amongst the beach’s cliffs. </p>
                                <p style="font-size:9px;">McCahon’s interest in poetry and biblical literature was to culminate during his final decade, resulting in his large scale word paintings which consisted purely of white text on a black ground.  These are amongst the artist’s most powerful works, their stark appearance and solemn passages imparting messages of both faith and doubt.</p>
                                <p style="font-size:9px;">Not only was McCahon a remarkable painter, but the critical thought and philosophical enquiry of his works carry great weight, continuing to resonate with viewers today. His works are held in all major public and private collections throughout New Zealand and in many collections and institutions worldwide.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row" style="background-color:#fff;">
            <div class="col-lg-7 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">

                        <div class="intro_text" style="text-align:justify;">
                            <p style="font-size:12px;"><b>Colin McCaho</b><br>Untitled (Dark Painting), 1965<br>oil on jute<br>960 x 915mm</p>
                        </div>
                        {{-- <div class="intro_link"><a href="rooms.html">View Rooms</a></div>
                        <a href="#" class="button_container intro_button"><div class="button text-center"><span>Book Your Stay</span></div></a> --}}
                    </div>
                </div>
            </div>
            <!-- Intro Image -->
            <div class="col-lg-5 intro_col">
                    <div class="intro_image magic_up">
                        <div style="text-align:right">
                            <img src="{{ URL::asset('front-theme/images/collections/oil_on_jute.jpg')}}" style="padding:10px;" alt="">
                        </div>
                    </div>
                </div>

        </div>


        <div class="row">
            <!-- Intro Image -->
            <div class="col-lg-7 intro_col">
                <div class="intro_image magic_up">
                    <div>
                        <img style="margin-top:100px;" src="{{ URL::asset('front-theme/images/collections/judy_millar.jpg')}}"  alt="">
                        {{-- <span style="line-height: -10px;font-size:9px;display:inline-block;padding-top:10px;">Hypnos, 2017   <br>charcoal on paper  <br>1270 x 750mm</span> --}}
                    </div>
                </div>
            </div>
            <!-- Intro Content -->
            <div class="col-lg-5 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section_title_container">
                                    <div class="section_title" style="text-align: right;"><h3>Judy<br><span style="color:#a8894c">Millar</span></h3></div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <p style="font-size:11px;">Central to Judy Millar’s painting practice is an ongoing investigation into the role of painting and its efficacy as means of communication.  More recent works are increasingly architectural and consider the confluences of painting, printing and threedimensionality.  With an established international exhibition history, Millar is one of New Zealand’s most internationally recognised and well-regarded artists. </p>
                                <p style="font-size:11px;">Millar represented New Zealand at the 53rd Venice Bienniale with her solo exhibition GiraffeBottle-Gun (2009) and exhibited in the Collateral Event Time, Space, Existence of the 54th Venice Biennale (2011). Her work was the subject of two major exhibitions at the Auckland Art Gallery (2002) and the IMA, Brisbane (2013). Her career continues to gain momentum in Europe, where she has shown at several museums including Rohkunstbau, Berlin (2010) and GASK, Gallery of Central Bohemia, Czech Republic (2013). Her paintings are held in all major public collections in New Zealand and in several international collections including the Kunstmuseum St Gallen and Tichy Foundation in Prague. </p>
                                <p style="font-size:11px;">Available monographs on her work include You You Me Me (Kerber Art, Germany, 2009) and Giraffe-Bottle Gun (Kerber Art, Germany, 2009).  In 2015 Millar co-produced the children’s popup book Swell. </p>
                                <p style="font-size:11px;">Gow Langsford Gallery represents Judy Millar and has done so since the gallery began in 1987.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row" style="background-color:#fff;margin-top:10px;">
            <div class="col-lg-7 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">

                        <div class="intro_text" style="text-align:justify;">
                            <p style="font-size:12px;"><b>Judy Millar</b><br>Untitled, 2013 <br>oil on canvas<br>1600 x 1350mm</p>
                        </div>
                        {{-- <div class="intro_link"><a href="rooms.html">View Rooms</a></div>
                        <a href="#" class="button_container intro_button"><div class="button text-center"><span>Book Your Stay</span></div></a> --}}
                    </div>
                </div>
            </div>
            <!-- Intro Image -->
            <div class="col-lg-5 intro_col">
                    <div class="intro_image magic_up">
                        <div style="text-align:right">
                            <img src="{{ URL::asset('front-theme/images/collections/oil_on_canvas.jpg')}}" alt="" style="padding:10px;">
                        </div>
                    </div>
                </div>

        </div>

        <div class="row">
            <!-- Intro Image -->
            <div class="col-lg-7 intro_col">
                <div class="intro_image magic_up">
                    <div>
                        <img style="margin-top:100px;" src="{{ URL::asset('front-theme/images/collections/michael_parekowhai.jpg')}}"  alt="">
                        {{-- <span style="line-height: -10px;font-size:9px;display:inline-block;padding-top:10px;">Hypnos, 2017   <br>charcoal on paper  <br>1270 x 750mm</span> --}}
                    </div>
                </div>
            </div>
            <!-- Intro Content -->
            <div class="col-lg-5 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section_title_container">
                                    <div class="section_title" style="text-align: right;"><h3>Micheal<br><span style="color:#a8894c">Parekowhai</span></h3></div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <p style="font-size:11px;">Michael Parekowhai draws upon an abundant range of both vernacular and collective vocabularies in his work. He re-manufactures these lexicons into complex narrative structures and formal languages, exploring perceptions of space, the ambiguities of identity, the shifting sensitivities of historical memory and the fluid relationship between art and craft. Ideas of camaraderie, tools of teaching and childhood learning, as well as quotes from modern art history and popular culture, also play out in many of Parekowhai’s stories. While his work is often described as emphasising the extraordinariness of the ordinary, each body of work has layers of potential for meaning and significance – they are open to any depth of interpretation and storytelling</p>
                                <p style="font-size:11px;">Michael Parekowhai was born in Porirua, New Zealand in 1968, of Maori (Ngati Whakarongo) and Pakeha descent. Parekowhai graduated with a BFA (1990) and MFA (2000) from the University of Auckland’s Elam School of Fine Arts, and in 2001 became an Arts Foundation of New Zealand Laureate. Notable exhibitions include a recent retrospective at the Queensland Art Gallery of Modern Art (2015), the Asia Pacific Triennial (2006-2007), the Gwangju Biennale (2004); Sydney Biennale (2002), Headlands, MCA, Sydney (1992). He currently holds the position of Professor in Fine Arts at the University of Auckland and lives and works in Auckland, New Zealand.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row" style="background-color:#fff;margin-top:10px;">

            <!-- Intro Image -->
            <div class="col-lg-12 intro_col">
                    <div class="intro_image magic_up">
                        <div style="text-align:right">
                            <img src="{{ URL::asset('front-theme/images/collections/rainbow_servant.jpg')}}" alt="" style="padding:10px;">
                        </div>
                    </div>
                </div>

        </div>

        <div class="row">
            <!-- Intro Image -->
            <div class="col-lg-7 intro_col">
                <div class="intro_image magic_up">
                    <div>
                        <img style="margin-top:100px;" src="{{ URL::asset('front-theme/images/collections/black_and_maroon_owl.jpg')}}"  alt="">
                        <span style="line-height: -10px;font-size:9px;display:inline-block;padding-top:10px;">Black and Maroon Owl, 1951   <br>glazed and painted ceramic vase <br>310 x 185 x 185mm</span>
                    </div>
                </div>
            </div>
            <!-- Intro Content -->
            <div class="col-lg-5 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section_title_container">
                                    <div class="section_title" style="text-align: right;"><h3>Pablo <br><span style="color:#a8894c">Picasso</span></h3></div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <p style="font-size:11px;">Pablo Ruiz Picasso (25 October 1881 – 8 April 1973) was a Spanish painter, sculptor, printmaker, ceramicist, stage designer, poet and playwright who spent most of his adult life in France. Regarded as one of the most influential artists of the 20th century, he is known for co-founding the Cubist movement, the invention of constructed sculpture, the coinvention of collage, and for the wide variety of styles that he helped develop and explore. Among his most famous works are the proto-Cubist Les Demoiselles d’Avignon (1907), and Guernica (1937), a dramatic portrayal of the bombing of Guernica by the German and Italian airforces during the Spanish Civil War.</p>
                                <p style="font-size:11px;">Picasso demonstrated extraordinary artistic talent in his early years, painting in a naturalistic manner through his childhood and adolescence. During the first decade of the 20th century, his style changed as he experimented with different theories, techniques, and ideas. After 1906, the Fauvist work of the slightly older artist Henri Matisse motivated Picasso to explore more radical styles, beginning a fruitful rivalry between the two artists, who subsequently were often paired by critics as the leaders of modern art. </p>
                                <p style="font-size:11px;">In 1946, Pablo Picasso visited the Madoura Pottery Workshop in Vallauris on the French Rivera where he became interested in the possibilities of ceramics and earthenware.  Following some initial experiments, Picasso was inspired by the results of his newfound medium and began what would become a long relationship with the pottery studio, frequently visiting and producing pieces until the end of his life in 1973. </p>
                                <p style="font-size:11px;">Ceramics enabled Picasso to experiment with the nuances between decoration and form in ways inaccessible through other media. His forms ranged from more sculptural pieces to vases, plates and plaques, and his subjects reflected those in his paintings and drawings - portraits, nature and animals. His pet owl and goat were also favourite motifs in his oeuvre at this time, as well as bullfighting scenes.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <!-- Intro Image -->
            <div class="col-lg-5 intro_col">
                <div class="intro_image magic_up">
                    <div>
                        <img style="margin-top:100px;" src="{{ URL::asset('front-theme/images/collections/john_pule.jpg')}}"  alt="">
                        <span style="line-height: -10px;font-size:9px;display:inline-block;padding-top:10px;">Black and Maroon Owl, 1951   <br>glazed and painted ceramic vase <br>310 x 185 x 185mm</span>
                    </div>
                </div>
            </div>
            <!-- Intro Content -->
            <div class="col-lg-7 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section_title_container">
                                    <div class="section_title" style="text-align: right;"><h3>John<br><span style="color:#a8894c">Pule</span></h3></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <p style="font-size:9px;">When John Pule first arrived in inner city Auckland as a young adult in 1980, the formal tenets of poetry and painting were largely unknown to him. Over the next 30 years Pule would explore new directions as both writer and painter, and has since emerged as one of this country’s most recognised painters and one of the most celebrated artists of the "New Oceania". His work is highly inventive, particularly in its adaptation of traditional Pacific art forms.  It is also challenging and provocative in content.  Pule writes, “Most ideas come from living things. The best ideas come from where I come from. Where I was born is a spectacular event in itself, and that is dazzling for me as an idea.” </p>
                                <p style="font-size:9px;">Niuean born, John Pule immigrated to New Zealand at the age of two in the early 1960s, visiting his birthplace as an adult in 1991. Since this initial visit Pule has returned to Niue on numerous occasions and both his personal experiences in Niue and the collective experiences of the Niuean people have become an outstanding theme in his artworks throughout his career.  Within his early works, Pule draws on the pre and post-colonial histories in the Pacific, largely focusing on the intricacies and impact of cultural imperialism and colonisation on the Niuean people. He explores the interactions between traditional Niuean mythology and Biblical stories exemplifying the obvious disconnect between the two as well as the disruption of Pacific culture due to colonisation.</p>
                            </div>
                            <div class="col-lg-6">
                                <p style="font-size:9px;">More recently, John Pule has focused his attention and artistic commentary to more contemporary and global issues. Within these works, Pule examines terror, the looming fear of wars, pollution and global politics whilst continuing to comment on cultural disruption. </p>
                                <p style="font-size:9px;">Since 1991 Pule has exhibited extensively throughout New Zealand, Australia, the Pacific and Asia. Pule has held residencies at the Cultural Museum in Rarotonga in 2003, and at Galerie Römerapotheke in Zurich in 2005. Pule’s work has been represented in two Asia-Pacific Triennials at the Queensland Art Gallery, and in 2004 he exhibited as part Paradise Now! at the Asia Society in New York - the largest exhibition of New Zealand art held since Te Maori in 1984. A published author, Pule has completed the three novels, including The Shark That Ate the Sun (1992) and Burn My Head in Heaven (1998), as well as being a published poet. In 2000 Pule was the University of Auckland Literary Fellow and in 2002 took up a distinguished-visiting writer residence
                                     in the department of English at the University of Hawaii.  In 2004 he was honoured with the prestigious Laureate Award from the Arts Foundation of New Zealand and in 2012 he was awarded an ONZM (Officer of the New Zealand Order of Merit) for services as an author, poet and painter in the Queen's Birthday Honours.  In 2010 City Gallery Wellington hosted Hauaga, a travelling survey exhibition of his career to date, which came to the Auckland Art Gallery in 2011/2012. In 2018 Pule’s five panel painting Kehe tau hauaga foou (To all new arrivals) was included as a feature work in London’s Royal Academy exhibition Oceania. </p>
                                <p style="font-size:9px;">John Pule has works held in major national and international collections including the Auckland Art Gallery, Te Papa Tongarewa, The National Gallery of Victoria and Queensland Art Gallery and inclusion in exhibitions at Museo de Atre Contemporaneo, Santiago, Chile (2012), Queensland Art Gallery, Brisbane (2011) and the Neuer Berliner Kunstverein, Berlin (2007). </p>
                                <p style="font-size:9px;">John Pule is represented by Gow Langsford Gallery.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row" style="background-color:#fff;margin-top:10px;">
            <div class="col-lg-7 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">

                        <div class="intro_text" style="text-align:justify;">
                            <p style="font-size:12px;"><b>John Pule</b><br>Nofo la Koe, 1999 <br>oil on canvas<br>2500 x 1805mm</p>
                        </div>
                        {{-- <div class="intro_link"><a href="rooms.html">View Rooms</a></div>
                        <a href="#" class="button_container intro_button"><div class="button text-center"><span>Book Your Stay</span></div></a> --}}
                    </div>
                </div>
            </div>
            <!-- Intro Image -->
            <div class="col-lg-5 intro_col">
                    <div class="intro_image magic_up">
                        <div style="text-align:right">
                            <img src="{{ URL::asset('front-theme/images/collections/nofo_la_koa.jpg')}}" alt="" style="padding:10px;">
                        </div>
                    </div>
                </div>

        </div>

        <div class="row">
            <!-- Intro Image -->
            <div class="col-lg-5 intro_col">
                <div class="intro_image magic_up">
                    <div>
                        <img style="margin-top:100px;" src="{{ URL::asset('front-theme/images/collections/clyde_scott.jpg')}}"  alt="">
                        {{-- <span style="line-height: -10px;font-size:9px;display:inline-block;padding-top:10px;">Black and Maroon Owl, 1951   <br>glazed and painted ceramic vase <br>310 x 185 x 185mm</span> --}}
                    </div>
                </div>
            </div>
            <!-- Intro Content -->
            <div class="col-lg-7 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section_title_container">
                                    <div class="section_title" style="text-align: right;"><h3>Clyde<br><span style="color:#a8894c">Scott</span></h3></div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <p style="font-size:11px;"><b>In the Wake of Captain Cook</b></p>
                                <p style="font-size:11px;">Clyde Scott's wife, Carol, is from the Barton family, owners of Barton & Sons Boat builders in St Mary’s Bay from 1901 until 1954. But the Bartons also had a secondary boatbuilding shed in Beachhaven; when Carol's uncle died in the late 90s, the Scotts made a discovery. </p>
                                <p style="font-size:11px;">"We went in to sort through the stuff in the shed, which was full of wonderful things," recalls Scott, "and that's when I discovered the strakes, the long, thin curved kauri strips which are used to make clinker dinghies." Charmed by the strakes' elegant shapes and smooth texture, Scott had his raw material for the paintings he wanted to make. </p>
                                <p style="font-size:11px;">His wife - an anthropology tutor at Auckland University - was again instrumental. Sitting at his kitchen table, Clyde opened her copy of Professor Anne Salmond's Two Worlds, the fascinating account of the first encounters between Maori and Pakeha from 1642-1772. </p>
                                <p style="font-size:11px;">The book includes long, thin coastline drawings by Herman Sporing, Joseph Banks' secretary aboard the Endeavour during its circumnavigation of New Zealand in 1769-70. </p>
                                <p style="font-size:11px;">And so Waterlines was born, long drawings on the strakes of the coastline charted by Cook, tinted with acrylic and gold text which explains the naming of the place or an event which happened there.</p>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row" style="background-color:#fff;margin-top:10px;">
            <div class="col-lg-3 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">

                        <div class="intro_text" style="text-align:justify;">
                            <p style="font-size:12px;"><b>Clyde Scott </b><br>39 Between Mana Island and Cape Terewhiti,<br>“Cook Straits” entrance, 2017 <br>acrylic and pencil <br>on dinghy kauri</p>
                        </div>
                        {{-- <div class="intro_link"><a href="rooms.html">View Rooms</a></div>
                        <a href="#" class="button_container intro_button"><div class="button text-center"><span>Book Your Stay</span></div></a> --}}
                    </div>
                </div>
            </div>
            <!-- Intro Image -->
            <div class="col-lg-9 intro_col">
                    <div class="intro_image magic_up">
                        <div style="text-align:right">
                            <img src="{{ URL::asset('front-theme/images/collections/39_between_mana.jpg')}}" alt="" style="padding:10px;">
                        </div>
                    </div>
                </div>

        </div>

        <div class="row">
            <!-- Intro Image -->
            <div class="col-lg-5 intro_col">
                <div class="intro_image magic_up">
                    <div>
                        <img style="margin-top:100px;" src="{{ URL::asset('front-theme/images/collections/her.jpg')}}"  alt="">
                        <span style="line-height: -10px;font-size:9px;display:inline-block;padding-top:10px;">Her, 2018<br>acrylic on canvas <br>1500 x 1000mm</span>
                    </div>
                </div>
            </div>
            <!-- Intro Content -->
            <div class="col-lg-7 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section_title_container">
                                    <div class="section_title" style="text-align: right;"><h3>Shao<br><span style="color:#a8894c">Tong</span></h3></div>
                                </div>
                            </div>
                            <div class="col-lg-5">

                                </div>
                            <div class="col-lg-7">
                                <p style="font-size:11px;">Shao Tong faces difficulty when making connections between objects in his field of view, everything seems to flow together but not be associated at all. Yet, he is curious of everything that surrounds him. When creating art he will isolate one element from his perspective and restrict his focus to the single element. He will conduct research and prepare manuscripts that revolve around his subject. But, the painting will never expose the expression of the subject; therefore, the audience can’t discern the emotion of the figure. This concealment of emotion ensures the viewer is always curious and connected to the painting.</p>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <!-- Intro Image -->
            <div class="col-lg-5 intro_col">
                <div class="intro_image magic_up">
                    <div>
                        <img style="margin-top:100px;" src="{{ URL::asset('front-theme/images/collections/john_walsh.jpg')}}"  alt="">
                        {{-- <span style="line-height: -10px;font-size:9px;display:inline-block;padding-top:10px;">Her, 2018<br>acrylic on canvas <br>1500 x 1000mm</span> --}}
                    </div>
                </div>
            </div>
            <!-- Intro Content -->
            <div class="col-lg-7 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section_title_container">
                                    <div class="section_title" style="text-align: right;"><h3>John <br><span style="color:#a8894c">Walsh</span></h3></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <p style="font-size:9px;">The work of Maori artist John Walsh has been described as “surreal images of intriguing figures…a cast of creatures who look only part human appear in transit between this world and the next.  Their means of transport is a floating futuristic helix with underworld tones while the characters… play out the dramas of life, love and lore.” (Ngahiraka Mason, Purangiaho: Seeing Clearly, exhibition catalogue, Auckland Art Gallery, 2001, p.35) </p>
                                <p style="font-size:9px;">Born in Tolaga Bay in New Zealand’s North Island, John Walsh spent much of his early days in the Gisborne region before travelling to Christchurch where he attended Ilam School of Fine Arts at Canterbury University between 1973 and 1974.  He later returned to the East Coast where he specialised in portraiture.  From the mid 70’s he worked on Marae restoration and related art projects before taking up teaching and curatorial  positions. </p>
                                <p style="font-size:9px;">It was his cultural upbringing and later access to both Maori taonga (treasures) and contemporary art while working as a curator at Te Papa Tongarewa Museum of New Zealand that provided the impetus for a new body of work in 2001.  Wrote artist Lisa Reihana – “I am surprised by this latest work, there is a new clarity of vision.  Still the restrained brushstrokes, but the figures are more defined, three dimensional and powerful, the characters carved out against their backgrounds.  The nostalgic sepia tones have given way to verdant bush greens and ocean blues.  Events are alive and happening.” (“Flipping & Gliding, Grinning & Flying – the art of John Walsh”, Art New Zealand, No. 101, 2002)</p>
                            </div>
                            <div class="col-lg-6">
                                <p style="font-size:9px;">“I think that a way into the other world is to look with fresh eyes at the past.  Even though the ancient world is a universe away from how we have to live now, most Maori would like to believe that we are still connected to the other world through our relationship with the ancestors.” (John Walsh, 2002) </p>
                                <p style="font-size:9px;">Walsh incorporates traditional Maori legends such is the case in Orokohanga – Genesis of 2002 (exhibited Pataka Art Museum, Porirua and John Leech Gallery, Auckland) which as a series of nine works tell the Maori story of creation.  Ranginui (sky father), Papatuanuka (earth mother), Hine Titama (goddess of the dawn) – all are characters in the rich tapestry of Walsh’s oevre.  Rather than depicting the characters as part of legends past, Walsh creates an ethereal mood and a sense of timelessness. </p>
                                <p style="font-size:9px;">To truly understand his work, an understanding of the Maori culture is needed.  Yet these works are beautiful and consequentially the viewer is drawn into them - learning the stories, learning the culture while enjoying the artwork itself.  These are works that encourage and inspire. </p>
                                <p style="font-size:9px;">Walsh works with oil on both canvas and board with the results often very different.  Works on board have a lushness and immediacy while works on canvas have more of a warmth and gentle beauty.  Colours are rich yet restrained.</p>
                                <p style="font-size:9px;">In the past few years Walsh has quickly gained a reputation as an artist to watch.  He has been included in significant curated exhibitions in recent years such as Purangiaho – Seeing Clearly at the Auckland Art Gallery Toi o Tamaki, 2001 and Parihaka – the Art of Passive Resistance at the City Gallery, Wellington, 2001.  Works in collections include Te Papa Tongarewa Museum of New Zealand, Wallace Arts Trust and the Tjibaou Cultural Centre, Noumea. </p>
                                <p style="font-size:9px;">Gow Langsford Gallery has represented John Walsh since 2001.</p>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row" style="background-color:#fff;margin-top:10px;">
            <div class="col-lg-3 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">

                        <div class="intro_text" style="text-align:justify;">
                            <p style="font-size:12px;"><b>John Walsh </b><br>Pare o tumatauenga, 2006 <br>oil on board<br>900 x 1200mm</p>
                        </div>
                        {{-- <div class="intro_link"><a href="rooms.html">View Rooms</a></div>
                        <a href="#" class="button_container intro_button"><div class="button text-center"><span>Book Your Stay</span></div></a> --}}
                    </div>
                </div>
            </div>
            <!-- Intro Image -->
            <div class="col-lg-9 intro_col">
                    <div class="intro_image magic_up">
                        <div style="text-align:right">
                            <img src="{{ URL::asset('front-theme/images/collections/pare_o_tumatauenga.jpg')}}" alt="" style="padding:10px;">
                        </div>
                    </div>
                </div>

        </div>

        <div class="row" style="background-color:#fff;margin-top:10px;">
            <!-- Intro Image -->
            <div class="col-lg-5 intro_col">
                <div class="intro_image magic_up">
                    <div style="text-align:left">
                        <img src="{{ URL::asset('front-theme/images/collections/world_view.jpg')}}" alt="" style="padding:10px;">
                    </div>
                </div>
            </div>
            <div class="col-lg-7 intro_col">
                    <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                        <div class="intro_content">

                            <div class="intro_text" style="text-align:justify;">
                                <p style="font-size:12px;"><b>Ottmar Horl</b><br>Worldview IV (standing) - Comment on Beuys, 2008  <br>edition of 250<br>plastic   <br>410 x 180 x 130mm</p>
                            </div>
                            {{-- <div class="intro_link"><a href="rooms.html">View Rooms</a></div>
                            <a href="#" class="button_container intro_button"><div class="button text-center"><span>Book Your Stay</span></div></a> --}}
                        </div>
                    </div>
                </div>


            </div>

    </div>
</div>


@endsection

@section('script')
<script src="{{ URL::asset('front-theme/js/about.js')}}"></script>

@endsection
