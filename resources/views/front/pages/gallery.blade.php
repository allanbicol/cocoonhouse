@extends('front.page-template')

@section('title')
<title>ART + WINE + DESIGN - Cocoon House</title>
@endsection

@section('meta')
<meta name="description" content="Our preferred caterers will quote you the full event experience including
food, bar service, staff, crockery, cutlery and glassware as required.">
<link rel="canonical" href="{{route('story')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="Cocoon Partners - Cocoon House">
<meta property="og:description" content="Our preferred caterers will quote you the full event experience including
food, bar service, staff, crockery, cutlery and glassware as required.">
<meta property="og:image" content="{{ URL::asset('front-theme/images/rosa.jpg')}}">
<meta property="og:url" content="{{route('story')}}">
<meta property="og:site_name" content="Cocoon House">

<meta name="twitter:title" content="Cocoon Partners - Cocoon House">
<meta name="twitter:description" content="Our preferred caterers will quote you the full event experience including
food, bar service, staff, crockery, cutlery and glassware as required.">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/rosa.jpg')}}">
<meta name="twitter:card" content="summary_large_image">
@endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/about_responsive.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/isotope.css')}}">
@endsection

@section('breadcrumb')
<div class="home">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{ URL::asset('front-theme/images/home_slider_8.jpg')}}" data-speed="0.8"></div>
		<div class="home_content">
			<div class="home_subtitle">Cocoon House</div>
			<div class="home_title">ART + WINE + DESIGN</div>
		</div>
    </div>
</div>
@endsection

@section('content')
<div class="intro">
    <div class="container">
        <div class="row">
            <div class="gallery">
                    <a target="_blank" href="img_5terre.jpg">
                            <div class="flip-container col4" ontouchstart="this.classList.toggle('hover');">
                                    <div class="flipper">
                                        <div class="front">
                                                <img  src="{{ URL::asset('front-theme/images/gallery/grid/img_1.jpg')}}"  >
                                        </div>
                                        <div class="back">
                                                Indulge in the traditional and beautiful Chinese Tea Ceremony right here at Cocoon House. In China, a tea ceremony is called “Jing Cha” meaning “to serve tea.” it also means “to be amazed or surprised with something.” Two different meanings, yet synonymous to each other.
                                        </div>
                                    </div>
                                </div>

                    </a>
                    {{-- <div class="desc">Add a description of the image here</div> --}}
                  </div>

                  <div class="gallery">
                    <a target="_blank" href="img_forest.jpg">
                            <div class="flip-container col4" ontouchstart="this.classList.toggle('hover');">
                                    <div class="flipper">
                                        <div class="front">
                                                <img  src="{{ URL::asset('front-theme/images/gallery/grid/img_2.jpg')}}"  >
                                        </div>
                                        <div class="back">
                                                Experience an authentic Chinese Tea Ceremony at Cocoon House’s Jiangnan Tea Room. Watch the historic ceremony unfold before your eyes carried out by a real Tea Master.
                                        </div>
                                    </div>
                                </div>
                    </a>
                    {{-- <div class="desc">Add a description of the image here</div> --}}
                  </div>

                  <div class="gallery">
                    <a target="_blank" href="img_lights.jpg">
                            <div class="flip-container col4" ontouchstart="this.classList.toggle('hover');">
                                    <div class="flipper">
                                        <div class="front">
                                                <img  src="{{ URL::asset('front-theme/images/gallery/grid/img_4.jpg')}}"  >
                                        </div>
                                        <div class="back">
                                                Everything in place for the tea ceremony. Shall we begin?
                                        </div>
                                    </div>
                                </div>
                    </a>
                    {{-- <div class="desc">Add a description of the image here</div> --}}
                  </div>

                  <div class="gallery">
                    <a target="_blank" href="img_mountains.jpg">
                            <div class="flip-container col4" ontouchstart="this.classList.toggle('hover');">
                                    <div class="flipper">
                                        <div class="front">
                                                <img  src="{{ URL::asset('front-theme/images/gallery/grid/wine_1.jpg')}}"  >
                                        </div>
                                        <div class="back">
                                                Winter Wine Tasting Series
                                        </div>
                                    </div>
                                </div>
                    </a>
                  </div>

                  <div class="gallery">
                        <a target="_blank" href="img_mountains.jpg">
                                <div class="flip-container col4" ontouchstart="this.classList.toggle('hover');">
                                        <div class="flipper">
                                            <div class="front">
                                                    <img  src="{{ URL::asset('front-theme/images/gallery/grid/wine_2.jpg')}}"  >
                                            </div>
                                            <div class="back">
                                                    Winter Wine Tasting Series
                                            </div>
                                        </div>
                                    </div>
                        </a>
                      </div>

                      <div class="gallery">
                            <a target="_blank" href="img_mountains.jpg">
                                    <div class="flip-container col4" ontouchstart="this.classList.toggle('hover');">
                                            <div class="flipper">
                                                <div class="front">
                                                        <img  src="{{ URL::asset('front-theme/images/gallery/grid/wine_3.jpg')}}"  >
                                                </div>
                                                <div class="back">
                                                        Winter Wine Tasting Series
                                                </div>
                                            </div>
                                        </div>
                            </a>
                          </div>

        </div>
    </div>
</div>


@endsection

@section('script')
<script src="{{ URL::asset('front-theme/js/about.js')}}"></script>

@endsection
