@extends('front.page-template')

@section('title')
<title>Home - Cocoon House</title>
@endsection

@section('meta')
<meta name="description" content="Cocoon House is the definition of a luxury oasis.Located at 35 Spring Street, Freemans Bay, the house is just a short stroll from Victoria Park. Every detail of the house has been lovingly curated to delight guests in an embrace of warmth, comfort and elegance.">
<link rel="canonical" href="{{route('homepage')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="Home - Cocoon House">
<meta property="og:description" content="Cocoon House is the definition of a luxury oasis.Located at 35 Spring Street, Freemans Bay, the house is just a short stroll from Victoria Park. Every detail of the house has been lovingly curated to delight guests in an embrace of warmth, comfort and elegance.">
<meta property="og:image" content="{{ URL::asset('front-theme/images/img_1.jpg')}}">
<meta property="og:url" content="{{route('homepage')}}">
<meta property="og:site_name" content="Cocoon House">

<meta name="twitter:title" content="Home - Cocoon House">
<meta name="twitter:description" content="Cocoon House is the definition of a luxury oasis.Located at 35 Spring Street, Freemans Bay, the house is just a short stroll from Victoria Park. Every detail of the house has been lovingly curated to delight guests in an embrace of warmth, comfort and elegance.">
<meta name="twitter:image" content="{{ URL::asset('front-theme/images/img_1.jpg')}}">
<meta name="twitter:card" content="summary_large_image">

@endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/main_styles.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/responsive.css')}}">
@endsection

@section('breadcrumb')
<div class="home">
    <!-- Home Slider -->
    <div class="home_slider_container" id="home_slider_container">
        <div class='slider'>
            <div class='slide1' style="background: url({{ URL::asset('front-theme/images/home_slider_1.jpg')}})no-repeat center;background-size: cover;"></div>
            <div class='slide2' style="background: url({{ URL::asset('front-theme/images/home_slider_2.jpg')}})no-repeat center;background-size: cover;"></div>
            <div class='slide3' style="background: url({{ URL::asset('front-theme/images/home_slider_3.jpg')}})no-repeat center;background-size: cover;"></div>
            <div class='slide4' style="background: url({{ URL::asset('front-theme/images/home_slider_4.jpg')}})no-repeat center;background-size: cover;"></div>
            <div class='slide5' style="background: url({{ URL::asset('front-theme/images/home_slider_5.jpg')}})no-repeat center;background-size: cover;"></div>
            <div class='slide6' style="background: url({{ URL::asset('front-theme/images/home_slider_6.jpg')}})no-repeat center;background-size: cover;"></div>
            <div class='slide7' style="background: url({{ URL::asset('front-theme/images/home_slider_7.jpg')}})no-repeat center;background-size: cover;"></div>
          </div>

      </div>
    </div>
</div>
@endsection


@section('content')
<!-- Intro -->

<div class="intro">
    <div class="container">
        <div class="row row-lg-eq-height">

            <!-- Intro Content -->
            <div class="col-lg-5 intro_col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-center magic_up">
                    <div class="intro_content">
                        <div class="section_title_container">
                            <div class="section_subtitle">Cocoon House</div>
                            <div class="section_title"><h4>WELCOME TO COCOON HOUSE</h4></div>
                        </div>
                        <div class="intro_text">
                            <div>Home to the Cocoon Salon of Art, Wine & Design</div>
                            <div>Home to an exquisite private art collection</div>
                            <div>Home to the serene Jiangnan Tea Ceremony</div>
                        </div>
                        <!-- <div class="intro_link"><a href="rooms.html">View Rooms</a></div> -->
                        <a href="{{ route('aboutus')}}" class="button_container intro_button"><div class="button text-center"><span>Find out more</span></div></a>
                    </div>
                </div>
            </div>

            <!-- Intro Image -->
            <div class="col-lg-7 intro_col">
                <div class="intro_images magic_up">
                    <div class="intro_1 intro_img"><img src="{{ URL::asset('front-theme/images/intro_1.jpg')}}" alt=""></div>
                    <div class="intro_2 intro_img"><img src="{{ URL::asset('front-theme/images/intro_2.jpg')}}" alt=""></div>
                    <div class="intro_3 intro_img"><img src="{{ URL::asset('front-theme/images/intro_3.jpg')}}" alt=""></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Big Room -->

<div class="big_room">
    <div class="container-fluid">
        <div class="row row-xl-eq-height">
            <div class="col-xl-6 order-xl-1 order-2">
                    <video width="100%" height="100%" controls style="background:#000;">
                        <source src="{{ URL::asset('front-theme/video/SocialMediaTrack.mp4')}}" type="video/mp4">
                        {{-- <source src="movie.ogg" type="video/ogg"> --}}
                        Your browser does not support the video tag.
                    </video>
                {{-- <div class="big_room_slider_container">

                    <div class="owl-carousel owl-theme big_room_slider">
                        <div class="owl-item">
                            <div class="background_image" style="background-image:url({{ URL::asset('front-theme/images/events/tea-ceremony/img_1.jpg')}})"></div>
                        </div>
                        <div class="owl-item">
                            <div class="background_image" style="background-image:url({{ URL::asset('front-theme/images/events/wine-tasting/wine_1.jpg')}})"></div>
                        </div>
                        <div class="owl-item">
                            <div class="background_image" style="background-image:url({{ URL::asset('front-theme/images/events/tea-ceremony/img_2.jpg')}})"></div>
                        </div>
                        <div class="owl-item">
                            <div class="background_image" style="background-image:url({{ URL::asset('front-theme/images/events/tea-ceremony/img_3.jpg')}})"></div>
                        </div>
                        <div class="owl-item">
                            <div class="background_image" style="background-image:url({{ URL::asset('front-theme/images/events/wine-tasting/wine_2.jpg')}})"></div>
                        </div>
                        <div class="owl-item">
                            <div class="background_image" style="background-image:url({{ URL::asset('front-theme/images/events/wine-tasting/wine_3.jpg')}})"></div>
                        </div>
                        <div class="owl-item">
                            <div class="background_image" style="background-image:url({{ URL::asset('front-theme/images/events/tea-ceremony/img_4.jpg')}})"></div>
                        </div>

                    </div>

                    <div class="big_room_slider_nav_container d-flex flex-row align-items-start justify-content-start">
                        <div class="big_room_slider_prev big_room_slider_nav"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
                        <div class="big_room_slider_next big_room_slider_nav"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
                    </div>
                </div> --}}
            </div>
            <div class="col-xl-6 order-xl-2 order-1">
                <div class="big_room_content">
                    <div class="big_room_content_inner magic_up">
                        <div class="section_title_container">
                            <!-- <div class="section_subtitle">luxury resort</div> -->
                            <div class="section_title" style="text-transform:uppercase;"><h4>Home to Auckland’s most exclusive private occasions</h4></div>
                        </div>
                        <div class="big_room_text">
                            <p>Cocoon House is the definition of a luxury oasis. Located at 35 Spring Street, Freemans Bay - just a short stroll from Victoria Park. Every detail of Cocoon has been lovingly curated to delight guests in an embrace of warmth, comfort and elegance.</p>

                            <p>Each room is a gallery of stunning, handcrafted furniture and taonga from New Zealand’s greatest contemporary artists. Cocoon House is a private sanctuary for exquisite boutique experiences.</p>

                            <p>The silk has been woven and the cocoon is fully prepared for your next escape from the outside world.
                            </p>
                        </div>
                        <!-- <div class="testimonial">
                            <div class="testimonial_stars">
                                <ul class="d-flex flex-row align-items-start justfy-content-start">
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                </ul>
                            </div>
                            <div class="testimonial_text">“ Praesent fermentum ligula in dui imperdiet, vel tempus nulla ultricies. Phasellus at commodo ligula. Nullam molestie volutpat sapien, a dignissim tortor laoreet quis.</div>
                            <div class="testimonial_author d-flex flex-row align-items-center justify-content-start">
                                <div class="testimonial_author_image"><img src="images/testimonial.png" alt=""></div>
                                <div class="testimonial_author_name"><a href="#">Michael Smith</a><span>, Client</span></div>
                            </div>
                        </div> -->
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Room -->

<div class="rooms">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="section_title_container text-center magic_up">
                    <div class="section_subtitle">Cocoon House</div>
                    <div class="section_title"><h2>COCOON SALON</h2></div>
                    <p>The  Cocoon Salon is situated on the first floor with each room opening to the central atrium. Inspired by the famous Hutong courtyards of ancient Beijing, this courtyard and lush tree are the centrepiece of the property and visible from nearly every room.</p>
                    <p>The Salon Floor encompasses the Dining Room and Formal Lounge, connected by the Atrium space.</p>
                </div>
            </div>
        </div>
        <div class="row room_row magic_up">

            <!-- Room -->
            <div class="col-lg-4 room_col">
                <div class="room">
                    <div class="room_image"><a href="{{route('salon1')}}"><img src="{{ URL::asset('front-theme/images/salon_1.jpg')}}" alt="THE DINING ROOM"></a></div>
                    <div class="room_content text-center">
                        {{-- <div class="room_price">$800.00 + GST</div> --}}
                        <div class="room_title"><a href="{{route('salon1')}}">DINING ROOM</a></div>
                        {{-- <div class="room_text">
                            <table class="table">
                                <tr>
                                    <td class="text-left"><i class="fa fa-money"></i> &nbsp;Price</td>
                                    <td class="text-right">$800.00 + gst<br><div style="font-size:8px;color:#000;">+ tea selections (inc. Tea Master)</td>
                                </tr>
                                <tr>
                                    <td class="text-left"><i class="fa fa-user"></i> &nbsp;Seated</td>
                                    <td class="text-right">8 pax</td>
                                </tr>
                            </table>
                        </div>
                        <a href="mailto:info@cocoonhouse.co.nz?subject=Cocoon House - Jiangnan Tea Room Booking" class="button_container room_button"><div class="button text-center"><span>Book Now</span></div></a> --}}
                    </div>
                </div>
            </div>

            <!-- Room -->
            <div class="col-lg-4 room_col">
                <div class="room">
                    <div class="room_image"><a href="{{route('salon2')}}"><img src="{{ URL::asset('front-theme/images/salon_2.jpg')}}" alt="THE ATRIUM"></a></div>
                    <div class="room_content text-center">
                        {{-- <div class="room_price">$500.00 + GST </div> --}}
                        <div class="room_title"><a href="{{route('salon2')}}">ATRIUM</a></div>
                        {{-- <div class="room_text">
                            <table class="table">
                                <tr>
                                    <td class="text-left"><i class="fa fa-money"></i> &nbsp;Price</td>
                                    <td class="text-right">$500.00 + gst</td>
                                </tr>
                                <tr>
                                    <td class="text-left"><i class="fa fa-briefcase"></i> &nbsp;Meeting</td>
                                    <td class="text-right">6 pax</td>
                                </tr>
                                <tr>
                                    <td class="text-left"><i class="fa fa-glass"></i> &nbsp;Cocktail</td>
                                    <td class="text-right">8 pax</td>
                                </tr>
                            </table>
                        </div>
                        <a href="mailto:info@cocoonhouse.co.nz?subject=Cocoon House - Fornasetti Library Booking" class="button_container room_button"><div class="button text-center"><span>Book Now</span></div></a> --}}
                    </div>
                </div>
            </div>

            <!-- Room -->
            <div class="col-lg-4 room_col">
                <div class="room">
                    <div class="room_image"><a href="{{route('salon3')}}"><img src="{{ URL::asset('front-theme/images/salon_3.jpg')}}" alt="THE FORMAL LOUNGE"></a></div>
                    <div class="room_content text-center">
                        {{-- <div class="room_price">$500.00 + GST </div> --}}
                        <div class="room_title"><a href="{{route('salon3')}}">FORMAL LOUNGE</a></div>
                        {{-- <div class="room_text">
                            <table class="table">
                                <tr>
                                    <td class="text-left"><i class="fa fa-money"></i> &nbsp;Price</td>
                                    <td class="text-right">$500.00 + gst</td>
                                </tr>
                                <tr>
                                    <td class="text-left"><i class="fa fa-cutlery"></i> &nbsp;Dining</td>
                                    <td class="text-right">8 pax</td>
                                </tr>
                                <tr>
                                    <td class="text-left"><i class="fa fa-briefcase"></i> &nbsp;Meeting</td>
                                    <td class="text-right">8 pax</td>
                                </tr>
                            </table>
                        </div>
                        <a href="mailto:info@cocoonhouse.co.nz?subject=Cocoon House - Boardroom Tu Booking" class="button_container room_button"><div class="button text-center"><span>Book Now</span></div></a> --}}
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- Gallery -->

<div class="gallery">
    <div style="text-align:center;font-size:30px;padding-bottom:20px;"><img src="{{ URL::asset('front-theme/images/instagram.png')}}" width="40px;"></div>
    <div class="gallery_slider_container">

        <!-- Gallery Slider -->
        {{-- <div class="owl-carousel owl-theme gallery_slider magic_up" id="instagram_images">
                @foreach($insta_images as $image)
                    @if ($image->type=='image')
                        <div class="owl-item gallery_item">
                            <div class="gallery_select d-flex flex-column align-items-center justify-content-center">
                                <div class="row" style="width:50%;">
                                    <div class="col-lg-6" style="padding-right:35px;">
                                        <div class="row">
                                            <div class="col-lg-6" style="padding:0px;"><img src="{{ URL::asset('front-theme/images/heart.png')}}"> </div>
                                            <div class="col-lg-6" style="font-size:20px;padding:0px;color:#fff;">&nbsp;{{$image->likes->count }}</div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6" style="padding-right:35px;">
                                        <div class="row">
                                            <div class="col-lg-6" style="padding:0px;"><img src="{{ URL::asset('front-theme/images/speech.png')}}"> </div>
                                            <div class="col-lg-6" style="font-size:20px;padding:0px;color:#fff;">{{$image->comments->count }}</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="insta_text" style="font-size:12px;color:#fff;"><p>{{$image->caption->text }}</p></div>
                            </div>
                        <a class="colorbox_" href="{{$image->link }}" target="_blank"><img src="{{$image->images->low_resolution->url }}" alt=""></a>
                        </div>
                    @endif
                @endforeach


        </div> --}}

    </div>

    <!-- Nav -->
    <div class="gallery_slider_nav_container">
        <div class="container">
            <div class="row">
                <div class="col clearfix">
                    <div class="gallery_slider_nav_content d-flex flex-row align-items-start justify-content-start">
                        <div class="gallery_slider_prev gallery_slider_nav"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
                        <div class="gallery_slider_next gallery_slider_nav"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection


@section('script')

<script src="{{ URL::asset('front-theme/js/custom.js')}}"></script>

@endsection
