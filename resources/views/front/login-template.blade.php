<!DOCTYPE html>
<html lang="en">
<head>
<title>Cocoon</title>
<link rel="icon" type="image/png" sizes="16x16" href="{{ URL::asset('front-theme/images/icon.png')}}">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Cocoon House is the definition of a luxury oasis.">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/styles/bootstrap-4.1.2/bootstrap.min.css')}}">
<link href="{{ URL::asset('front-theme/plugins/font-awesome-4.7.0/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('front-theme/plugins/colorbox/colorbox.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/plugins/OwlCarousel2-2.2.1/owl.carousel.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/plugins/OwlCarousel2-2.2.1/owl.theme.default.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('front-theme/plugins/OwlCarousel2-2.2.1/animate.css')}}">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" type="text/css">

@yield('style')

</head>
<body>

<div class="super_container">

	<!-- Header -->

	<header class="header">
		<div class="header_content">

			<!-- Logo -->
			<div class="logo_container d-flex flex-column align-items-center justify-content-center">
				<div class="logo">
					<a href="#" class="text-center">
						<!-- <div class="logo_subtitle">hotel</div>
						<div class="logo_title">Samira</div>
						<div class="logo_stars">
							<ul class="d-flex flex-row align-items-start justfy-content-start">
								<li><i class="fa fa-star" aria-hidden="true"></i></li>
								<li><i class="fa fa-star" aria-hidden="true"></i></li>
								<li><i class="fa fa-star" aria-hidden="true"></i></li>
								<li><i class="fa fa-star" aria-hidden="true"></i></li>
								<li><i class="fa fa-star" aria-hidden="true"></i></li>
							</ul>
						</div> -->
						<img src="{{ URL::asset('front-theme/images/Cocoon-House-Gold.png')}}" width="100px"/>
					</a>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="header_inner d-flex flex-row align-items-center justify-content-start">
							<nav class="main_nav">
								<ul class="d-flex flex-row align-items-center justify-content-start">
                                    <li @if(Session::get('nav')=='index') class="active" @endif><a href="{{ route('homepage') }}"><div class="nav_item d-flex flex-column align-items-center justify-content-center"><span>home</span></div></a></li>
									<li @if(Session::get('nav')=='aboutus') class="active" @endif><a href="{{ route('aboutus') }}"><div class="nav_item d-flex flex-column align-items-center justify-content-center"><span>about</span></div></a></li>
                                    <li @if(Session::get('nav')=='spaces') class="active" @endif><a href="{{route('spaces')}}"><div class="nav_item d-flex flex-column align-items-center justify-content-center"><span>Cocoon Spaces</span></div></a></li>
                                    <li @if(Session::get('nav')=='story') class="active" @endif><a href="{{route('story')}}"><div class="nav_item d-flex flex-column align-items-center justify-content-center"><span>Rosa's Story</span></div></a></li>
									<li @if(Session::get('nav')=='find') class="active" @endif><a href="{{route('find')}}"><div class="nav_item d-flex flex-column align-items-center justify-content-center"><span>Find Cocoon</span></div></a></li>
									<li @if(Session::get('nav')=='contact') class="active" @endif><a href="{{route('contact')}}"><div class="nav_item d-flex flex-column align-items-center justify-content-center"><span>contact</span></div></a></li>
								</ul>
							</nav>
							<a href="#" class="button_container header_button ml-auto"><div class="button text-center btn-header"><span>Member Login</span></div></a>
							<div class="hamburger ml-auto"><i class="fa fa-bars" aria-hidden="true"></i></div>
							<!-- <div class="header_review"><a href="#">Add your review</a></div> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<!-- Menu -->

	<div class="menu">
		<div class="background_image" style="background-image:url(images/menu.jpg)"></div>
		<div class="menu_content d-flex flex-column align-items-center justify-content-center">
			<ul class="menu_nav_list text-center">
				<li><a href="{{route('homepage')}}">Home</a></li>
				<li><a href="{{route('aboutus')}}">About</a></li>
                <li><a href="{{route('spaces')}}">Cocoon Spaces</a></li>
                <li><a href="{{route('story')}}">Rosa's Story</a></li>
				<li><a href="{{route('find')}}">Find Cocoon</a></li>
				<li><a href="{{route('contact')}}">Contact</a></li>
			</ul>
		</div>
	</div>

    @yield('content')



</div>

<script src="{{ URL::asset('front-theme/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{ URL::asset('front-theme/styles/bootstrap-4.1.2/popper.js')}}"></script>
<script src="{{ URL::asset('front-theme/styles/bootstrap-4.1.2/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('front-theme/plugins/greensock/TweenMax.min.js')}}"></script>
<script src="{{ URL::asset('front-theme/plugins/greensock/TimelineMax.min.js')}}"></script>
<script src="{{ URL::asset('front-theme/plugins/scrollmagic/ScrollMagic.min.js')}}"></script>
<script src="{{ URL::asset('front-theme/plugins/greensock/animation.gsap.min.js')}}"></script>
<script src="{{ URL::asset('front-theme/plugins/greensock/ScrollToPlugin.min.js')}}"></script>
<script src="{{ URL::asset('front-theme/plugins/colorbox/jquery.colorbox-min.js')}}"></script>
<script src="{{ URL::asset('front-theme/plugins/OwlCarousel2-2.2.1/owl.carousel.js')}}"></script>
<script src="{{ URL::asset('front-theme/plugins/easing/easing.js')}}"></script>
<script src="{{ URL::asset('front-theme/plugins/parallax-js-master/parallax.min.js')}}"></script>
@yield('script')
</body>
</html>
