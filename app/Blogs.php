<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blogs extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'blogs';

    protected $fillable = ['title','is_draft','is_visible','details','category','featured_img','tags','date_created'];

    public $timestamps = false;
}
