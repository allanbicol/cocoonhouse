<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Projects;
use App\ProjectCategory;
use Carbon\Carbon;

class ProjectsController extends Controller
{
    function index()
    {
        $projects = Projects::all();

        return view('back.main.projects.index',['projects'=>$projects]);
    }

    function add()
    {
        $projectCategory = ProjectCategory::all();

        return view('back.main.projects.add',['categories'=>$projectCategory]);
    }

    function addAction(Request $request)
    {
        $this->validate($request,[
            'name' => 'required'
        ]);

        $project =  new Projects();

        $project->name                  =   $request->input('name');
        $project->client_name           =   $request->input('client_name');
        $project->website               =   $request->input('website');
        $project->description           =   $request->input('details');
        $project->category              =   json_decode($request->input('category'));
        $project->project_thumbnail     =   '1';
        $project->created_at            =   Carbon::now();
        $project->save();

        return redirect('/admin/projects');
    }

    function category(){
        $categories = ProjectCategory::orderBy('id','desc')
                        ->get();

        return view('back.main.projects.category',['categories'=>$categories]);
    }

    function addCategory(Request $request)
    {
        $this->validate($request,[
            'name'  =>  'required',
            'slug'  =>  'required'
        ]);

        // $category = new ProjectsCategory();
        // $category->name = $request->input('name');
        // $category->slug = $request->input('slug');
        // $category->description = $request->input('description');
        $saveCategory = ProjectCategory::create($request->all());

        if($saveCategory){
            return back()->with('success','New category has been added.');
        }
    }
}
