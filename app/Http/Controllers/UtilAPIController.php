<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UtilAPIController extends Controller
{
    public function addSubscriber($email){
        $url = "https://app.sender.net/api/";

        $data = array(
            "method" => "listSubscribe",
            "params" => [
                "api_key" => "99ed9bf963098d9e95a60701c3442f9a",
                "list_id" => "88042",
                "emails" => $email
            ]
        );
        $query = http_build_query(array('data' => json_encode($data)));

        $options = [
            'http' => [
                'header' => "Content-Type: application/x-www-form-urlencoded\r\n".
                    "Content-Length: ".strlen($query)."\r\n".
                    "User-Agent:MyAgent/1.0\r\n",
                'method'  => 'POST',
                'content' => $query,
            ]
        ];
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);

        return $result;
    }

    public function registerToRWCEvent($data){
        $url = "https://app.sender.net/api/";

        $data = array(
            "method" => "listSubscribe",
            "params" => [
                "api_key" => "99ed9bf963098d9e95a60701c3442f9a",
                "list_id" => "97427",
                "emails" => $data
            ]
        );
        $query = http_build_query(array('data' => json_encode($data)));

        $options = [
            'http' => [
                'header' => "Content-Type: application/x-www-form-urlencoded\r\n".
                    "Content-Length: ".strlen($query)."\r\n".
                    "User-Agent:MyAgent/1.0\r\n",
                'method'  => 'POST',
                'content' => $query,
            ]
        ];
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);

        return $result;
    }
}
