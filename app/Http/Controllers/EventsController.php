<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Events;
use DB;
use Mail;

class EventsController extends Controller
{
    function events(){
        Session::put('nav', 'gallery');
        $events = Events::orderBy('id','asc')
                        ->get();

        return view('front.pages.events',['events'=>$events]);

    }

    function viewRWCDetails($id){
        $event = DB::table('tbl_events')->where('id',$id)->first();
        echo 'aasdasdas';

        return view('front.pages.events.RWC.details',['event'=>$event]);
    }

    function registerRWCByEvent(Request $request){
        $data = [
            'fname'=>$request->input('fname'),
            'lname'=>$request->input('lname'),
            'email'=>$request->input('email'),
            'phone'=>$request->input('phone'),
            'no_of_attendees'=>$request->input('number_of_attendees'),
            'company'=>$request->input('company'),
            'event'=>$request->input('event'),
            'image'=>$request->input('image'),
        ];
        // dump($data);

        // return view('front.mail.rwc-email',['data'=>$data]);
        // exit();

    //    Mail::to('bicolallan@gmail.com')->send(new SendMail($data));
        $data1 = array(array(
            'email'=>$request->input('email'),
            'full_name'=>$request->input('fname').' '.$request->input('lname'),
            'phone'=>$request->input('phone'),
            'no_of_attendee'=>$request->input('number_of_attendees'),
            'company'=>$request->input('company'),
            'event_name'=>$request->input('event')
        ));
        $list = new UtilAPIController;

        $result = $list->registerToRWCEvent($data1);
        $result = json_decode($result);

        // (isset($result->error)) ? $respone = 0 : $respone = 1;

        $email = Mail::send('front.mail.rwc-email', ['data'=>$data], function($message) use ($request) {
            $message->to('info@cocoonhouse.co.nz');
            $message->from('admin@cocoonhouse.co.nz',"Cocoon House");
            $message->subject('Registration: '.$request->input('event'));
        });

        return back()->with('success',"You're successfully registered!");
    }
}
