<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontPortfolioController extends Controller
{
    function room1()
    {
        return view('front.pages.rooms.salon1');
    }
    function room2()
    {
        return view('front.pages.rooms.salon2');
    }
    function room3()
    {
        return view('front.pages.rooms.salon3');
    }

    function room4()
    {
        return view('front.pages.rooms.top1');
    }
    function room5()
    {
        return view('front.pages.rooms.top2');
    }
    function room6()
    {
        return view('front.pages.rooms.top3');
    }
    function room7()
    {
        return view('front.pages.rooms.ground1');
    }
}
