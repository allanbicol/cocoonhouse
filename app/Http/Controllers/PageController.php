<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\SendMail;
use Mail;
use Session;
use App\Rules\Captcha;

class PageController extends Controller
{
    function index()
    {
        // $url = "https://api.instagram.com/v1/users/self/media/recent/";
        // $url = $url . "?access_token=13234797819.1677ed0.10eefd5e114c4f1bbb393fc68aadd158";
        // $url = $url . "&count=8";
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, $url);                                                                                                                         curl_setopt($ch, CURLOPT_POST, 0);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // $response = curl_exec ($ch);
        // $err = curl_error($ch);  //if you need
        // curl_close ($ch);
        // print_r($response);
        // exit();
        // $data = json_decode($response);
        // $response= json_encode($data->data);

        // $data = json_decode($response,false);
        Session::put('nav', 'index');
        // return view('front.pages.index', array('insta_images'=>$data));
        return view('front.pages.index');
    }

    function aboutus()
    {
        Session::put('nav', 'aboutus');
        return view('front.pages.aboutus');
    }

    function portfolio()
    {
        Session::put('nav', 'portfolio');
        return view('front.pages.portfolio');
    }

    function spaces()
    {
        Session::put('nav', 'spaces');
        return view('front.pages.spaces');
    }

    function contact()
    {
        Session::put('nav', 'contact');
        return view('front.pages.contact');
    }

    function find(){
        Session::put('nav', 'contact');
        return view('front.pages.find');
    }
    function story(){
        Session::put('nav', 'aboutus');
        return view('front.pages.story');
    }

    function partners(){
        Session::put('nav', 'partners');
        return view('front.pages.partners');
    }
    function gallery(){
        Session::put('nav', 'gallery');
        return view('front.pages.gallery1');
    }

    function instagramfeed(){
        // $url = "http://85.187.132.83:8086/insta";
        $url = "https://api.instagram.com/v1/users/self/media/recent/";
        $url = $url . "?access_token=13234797819.1677ed0.10eefd5e114c4f1bbb393fc68aadd158";
        $url = $url . "&count=8";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec ($ch);
        $err = curl_error($ch);  //if you need
        curl_close ($ch);
        // print_r($response);
        // exit();
        $data = json_decode($response);
        //print_r(json_encode($data->data[1]));
        return json_encode($data->data);


    }

    function login(){
        Session::put('nav', 'login');
        return view('front.pages.login');
    }

    function sendEmail(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'g-recaptcha-response' => new Captcha()
        ]);

        $data = [
            'name'=>$request->input('name'),
            'email'=>$request->input('email'),
            'phone'=>$request->input('phone'),
            'attendees'=>$request->input('attendees'),
            'date'=>$request->input('date'),
            'subject'=>$request->input('subject'),
            'message'=>$request->input('message'),
        ];
        // dump($data);
        // print_r($data['services']);
        // return view('front.mail.contact',['data'=>$data]);
        // exit();

    //    Mail::to('bicolallan@gmail.com')->send(new SendMail($data));

        $email = Mail::send('front.mail.contact', ['data'=>$data], function($message) {
            $message->to('info@cocoonhouse.co.nz');
            $message->from('admin@cocoonhouse.co.nz',"Cocoon House");
            $message->subject('Cocoon Contact Us Inquiry');
        });

        return back()->with('success','Message sent successfully!');
    }

    function newsletterSubscriber(Request $request){

        $email = array();
        array_push($email, $request->input('email'));

        $list = new UtilAPIController;

        $result = $list->addSubscriber($email);
        $result = json_decode($result);
        (isset($result->error)) ? $respone = 0 : $respone = 1;

        return $respone;

    }
}
