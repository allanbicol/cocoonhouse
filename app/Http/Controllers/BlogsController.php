<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BlogCategory;
use Validator;

class BlogsController extends Controller
{
    function category(){
        $categories = BlogCategory::orderBy('id','desc')
                        ->get();

        return view('back.main.blogs.category',['categories'=>$categories]);
    }

    function addCategory(Request $request)
    {
        $this->validate($request,[
            'name'  =>  'required',
            'slug'  =>  'required'
        ]);

        $category = new BlogCategory();
        $category->name = $request->input('name');
        $category->slug = $request->input('slug');
        $category->description = $request->input('description');
        $saveCategory = $category->save();

        if($saveCategory){
            return back()->with('success','New category has been added.');
        }
    }
}
