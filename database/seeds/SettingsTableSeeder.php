<?php

use Illuminate\Database\Seeder;
use App\Settings;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Settings::create([
            'sitename'  =>  'hunt creative',
            'slogan'    =>  'All you need is love mixed with creativity'
        ]);
    }
}
